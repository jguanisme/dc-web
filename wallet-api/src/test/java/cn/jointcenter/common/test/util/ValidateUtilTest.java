package cn.jointcenter.common.test.util;

import cn.jointcenter.common.test.base.AbstractSpringTest;
import cn.jointcenter.common.util.ValidateUtil;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;

@EnableAutoConfiguration
public class ValidateUtilTest extends AbstractSpringTest {

    @Test
    public void phoneTest(){
        String ret = ValidateUtil.isPhone("17110000000");
        System.out.println(ret);
        Assert.assertEquals("", ret);

        String ret1 = ValidateUtil.isPhone("sss");
        System.out.println(ret1);
        Assert.assertNotEquals("", ret1);

        String ret2 = ValidateUtil.isPhone("17410000000");
        System.out.println(ret2);
        Assert.assertNotEquals("", ret2);
    }
}
