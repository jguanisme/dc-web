package cn.jointcenter.common.test.base;

//import cn.jointcenter.common.util.RedisConfig;
import org.junit.runner.RunWith;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.boot.autoconfigure.orm.jpa.HibernateJpaAutoConfiguration;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Import;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.junit4.SpringRunner;

@ContextConfiguration(locations = {"classpath:applicationContext-test.xml"})
@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
@Import({
        //RedisConfig.class
})
public abstract class AbstractSpringTest {

    String classname;

    public AbstractSpringTest() {
        super();
        classname = getClass().getName();
    }
}


