//-----------------------------------------------------------------------------
//      @Author     : GuanJie XiaoJunfeng ShiYunling(Diana SHI)
//      @License    : GNU General Public License, version 3 (GPL-3.0)
//
//      Copyright (C)2018 GuanJie XiaoJunfeng ShiYunling(Diana SHI)
//
//      This program is open source: you can redistribute it and/or modify
//      it under the terms of the GNU General Public License as published by
//      the Free Software Foundation, either version 3 of the License, or
//      (at your option) any later version.
//
//      This program is distributed in the hope that it will be useful,
//      but WITHOUT ANY WARRANTY; without even the implied warranty of
//      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
//      GNU General Public License for more details.
//
//      You should have received a copy of the GNU General Public License
//      along with this program.If not, see < https://www.gnu.org/licenses/>.
//-----------------------------------------------------------------------------
package cn.jointcenter.wallet.api.base;

import cn.jointcenter.common.util.RestUtil;
import cn.jointcenter.common.util.SecurityUtil;
import cn.jointcenter.common.util.ValidateUtil;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;

import javax.servlet.http.HttpServletRequest;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.Date;
import java.util.Locale;
import java.util.Random;

public class BaseController {

    protected String CAPTCHA_PREFIX = "Captcha:";
    protected String RECHARGE_PREFIX = "Recharge:Today:";
    protected String RECHARGE_SWITCH_KEY = "Recharge:Switch";
    protected String RECHARGE_TEST_KEY = "Recharge:TestModel";

    @Autowired
    protected HttpServletRequest request;

    @Autowired
    protected SecurityUtil securityUtil;

    @Autowired
    private RedisTemplate<String, String> redisTemplate;

    protected boolean isTesting(){
//        return false;
        //TODO: comment this snippet in prod
        String rechargeTest = redisTemplate.opsForValue().get(RECHARGE_TEST_KEY);
        return "TESTING_SKIP_RECHARGE".equals(rechargeTest);
    }

    /**
     * get login user wallet address
     * @return int
     */
//    protected String getUserId(){
//        String token = request.getHeader("token");
//        if(StringUtils.isBlank(token)){
//            return "";
//        }
//        String userId = securityUtil.checkParameterToken(token);
//        if(StringUtils.isBlank(userId)){
//            return "";
//        }
//        return userId;
//    }

    protected String getUTXO(){
        String token = request.getHeader("token");
        if(StringUtils.isBlank(token)){
            return "";
        }
        String utxo = securityUtil.checkParameterToken(token);
        if(StringUtils.isBlank(utxo)){
            return "";
        }
        return utxo.split(",")[0];
    }

    protected Integer getWalletId(){
        String token = request.getHeader("token");
        if(StringUtils.isBlank(token)){
            return 0;
        }
        String utxo = securityUtil.checkParameterToken(token);
        if(StringUtils.isBlank(utxo)){
            return 0;
        }
        return Integer.valueOf(utxo.split(",")[1]);
    }

    protected LocalDateTime convertToDateTime(String time){
        DateTimeFormatter dateTimeFormatter = DateTimeFormatter
                .ofPattern("yyyy-MM-dd HH:mm:ss");
        return LocalDateTime.parse(time.replace("T", " ")
                .replace("+08:00",""), dateTimeFormatter);
    }

    /**
     * compare a bill is expired or not
     * @param time
     * @return
     */
    protected Boolean billExpired(String time){
        LocalDateTime dateTime = convertToDateTime(time);
        return LocalDateTime.now().compareTo(dateTime.plusSeconds(120))>0;
    }

    /**
     * compare a bill is expired or not
     * @param time
     * @return
     */
    protected Boolean billExpiredForReceipt(String time){
        LocalDateTime dateTime = convertToDateTime(time);
        return LocalDateTime.now().compareTo(dateTime.plusSeconds(30))>0;
    }

    /**
     * format datetime
     * @param time 2018-08-08T20:00:00+08:00
     * @return 2018-08-08 20:00:00
     */
    protected String formatDateTimeWithZone(String time){
        return time.replace("T", " ").replace("+08:00","");
    }

    protected String genOrderId(){
        return new SimpleDateFormat("yyyyMMddHHmmssSSS").format(new Date()) + randomStr(4);
    }

    protected Integer getRemainSecondsToday() {
        Date currentDate = new Date();
        LocalDateTime midnight = LocalDateTime.ofInstant(currentDate.toInstant(),
                ZoneId.systemDefault()).plusDays(1).withHour(0).withMinute(0)
                .withSecond(0).withNano(0);
        LocalDateTime currentDateTime = LocalDateTime.ofInstant(currentDate.toInstant(),
                ZoneId.systemDefault());
        long seconds = ChronoUnit.SECONDS.between(currentDateTime, midnight);
        return (int) seconds;
    }

    private String randomStr(int len){
        java.util.Random r=new java.util.Random(10);
        String s = "";
        for(int i=0;i<len;i++){
            s += String.valueOf(r.nextInt());
        }
        return s;
    }

    protected RestUtil checkMobile(String mobile){
        if(StringUtils.isBlank(mobile)){
            return restFail(RestCode.PARAM_ERROR);
        }

        String ret = ValidateUtil.isPhone(mobile);
        if(StringUtils.isNotBlank(ret)){
            return RestUtil.error(RestCode.VALIDATE_ERROR.getCode(), ret);
        }
        if(mobile.startsWith("170") || mobile.startsWith("171")){
            return RestUtil.error(RestCode.VALIDATE_ERROR.getCode(), "不支持虚拟手机号");
        }

        return RestUtil.success("", "");
    }

    protected RestUtil restFail(RestCode restCode){
        return RestUtil.error(restCode.getCode(), restCode.getMessage());
    }
}
