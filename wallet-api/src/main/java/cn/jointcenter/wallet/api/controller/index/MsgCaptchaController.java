//-----------------------------------------------------------------------------
//      @Author     : GuanJie XiaoJunfeng ShiYunling(Diana SHI)
//      @License    : GNU General Public License, version 3 (GPL-3.0)
//
//      Copyright (C)2018 GuanJie XiaoJunfeng ShiYunling(Diana SHI)
//
//      This program is open source: you can redistribute it and/or modify
//      it under the terms of the GNU General Public License as published by
//      the Free Software Foundation, either version 3 of the License, or
//      (at your option) any later version.
//
//      This program is distributed in the hope that it will be useful,
//      but WITHOUT ANY WARRANTY; without even the implied warranty of
//      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
//      GNU General Public License for more details.
//
//      You should have received a copy of the GNU General Public License
//      along with this program.If not, see < https://www.gnu.org/licenses/>.
//-----------------------------------------------------------------------------
package cn.jointcenter.wallet.api.controller.index;

import cn.jointcenter.account.service.AccountService;
import cn.jointcenter.common.annotation.LoginRequired;
import cn.jointcenter.common.util.RestUtil;
import cn.jointcenter.wallet.api.base.BaseController;
import cn.jointcenter.wallet.api.base.RestCode;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.concurrent.TimeUnit;


@Api(value="Msg Captcha Controller", description="短信验证码专用", tags={"Msg Captcha Controller"})
@RestController
@RequestMapping(value="/api", produces = {"application/json;charset=UTF-8"})
public class MsgCaptchaController extends BaseController {
    private final  AccountService accountService;
    private final RedisTemplate<String, String> redisTemplate;

    @Autowired
    public MsgCaptchaController(AccountService accountService, RedisTemplate<String, String> redisTemplate) {
        this.accountService = accountService;
        this.redisTemplate = redisTemplate;
    }

    @ApiOperation(value="获取CSRF", notes="获取CSRF")
    @PostMapping(value = "/v1/csrf")
    public RestUtil getCsrf(){
        return RestUtil.success("", securityUtil.generalCsrf());
    }

    @ApiOperation(value="获取注册的短信码", notes="获取注册的短信码")
    @ApiImplicitParams({
            @ApiImplicitParam(name="mobile", value="手机号", dataType="String", paramType = "query"),
            @ApiImplicitParam(name="csrf", value="CSRF", dataType="String", paramType = "query")
    })
    @PostMapping(value = "/v1/captcha/reg")
    public RestUtil getForRegister(String mobile, String csrf){
        if(!securityUtil.checkCsrf(csrf)){
            return restFail(RestCode.PERMISSION_DENY);
        }

        RestUtil rest = this.checkMobile(mobile);
        if(1!=rest.getCode()){
            return rest;
        }

        if(isTesting()) {
            redisTemplate.opsForValue().set(CAPTCHA_PREFIX + mobile, "111111", 600L, TimeUnit.SECONDS);
        }
        else{
            //send sms
            String sms = accountService.sendSms(mobile);
            if (StringUtils.isBlank(sms)) {
                return restFail(RestCode.SMS_SEND_ERROR);
            }
            redisTemplate.opsForValue().set(CAPTCHA_PREFIX + mobile, sms, 600L, TimeUnit.SECONDS);
        }

        return RestUtil.success("", "");
    }


    @ApiOperation(value="获取登录后的短信码", notes="获取登录后操作的短信码，会验证token")
    @ApiImplicitParams({
            @ApiImplicitParam(name="mobile", value="手机号", dataType="String", paramType = "query")
    })
    @LoginRequired
    @PostMapping(value = "/v1/captcha/auth")
    public RestUtil getForAuth(String mobile) {
        Integer id = this.getWalletId();
        if (id <= 0) {
            return restFail(RestCode.NEED_LOGIN);
        }

        RestUtil rest = this.checkMobile(mobile);
        if(1!=rest.getCode()){
            return rest;
        }

        if(isTesting()) {
            redisTemplate.opsForValue().set(CAPTCHA_PREFIX + mobile, "111111", 600L, TimeUnit.SECONDS);
        }
        else {
            //send sms
            String sms = accountService.sendSms(mobile);
            if (StringUtils.isBlank(sms)) {
                return restFail(RestCode.SMS_SEND_ERROR);
            }
            redisTemplate.opsForValue().set(CAPTCHA_PREFIX + mobile, sms, 600L, TimeUnit.SECONDS);
        }
        return RestUtil.success("", "");
    }
}
