//-----------------------------------------------------------------------------
//      @Author     : GuanJie XiaoJunfeng ShiYunling(Diana SHI)
//      @License    : GNU General Public License, version 3 (GPL-3.0)
//
//      Copyright (C)2018 GuanJie XiaoJunfeng ShiYunling(Diana SHI)
//
//      This program is open source: you can redistribute it and/or modify
//      it under the terms of the GNU General Public License as published by
//      the Free Software Foundation, either version 3 of the License, or
//      (at your option) any later version.
//
//      This program is distributed in the hope that it will be useful,
//      but WITHOUT ANY WARRANTY; without even the implied warranty of
//      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
//      GNU General Public License for more details.
//
//      You should have received a copy of the GNU General Public License
//      along with this program.If not, see < https://www.gnu.org/licenses/>.
//-----------------------------------------------------------------------------
package cn.jointcenter.wallet.api;

import cn.jointcenter.common.http.OkHttpUtils;
import cn.jointcenter.common.util.impl.EncryptConverter;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.boot.autoconfigure.orm.jpa.HibernateJpaAutoConfiguration;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Import;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.io.File;
import java.util.Arrays;

@Slf4j
@ComponentScan(basePackages = {
        "cn.jointcenter.wallet.api",
        "cn.jointcenter.common",
        "cn.jointcenter.account.service",
        "cn.jointcenter.wallet.service",
        "com.unionpay.acp.sdk"})
@SpringBootApplication(exclude = {
        DataSourceAutoConfiguration.class,
        HibernateJpaAutoConfiguration.class
})
@Import({
//        DataSourceConfiguration.class,
//        MybatisConfiguration.class
})
//@EnableAutoConfiguration(exclude={DruidDataSourceAutoConfigure.class})
@EnableSwagger2
public class ApiRunner {

    @Value("${dc.gateway.base-protocol}")
    private String gatewayBaseProtocol;

    @Value("${dc.gateway.base-domain}")
    private String gatewayBaseDomain;

    @Value("${dc.gateway.base-port}")
    private String gatewayBasePort;

    @Value("${dc.shop.base-url}")
    private String shopBaseUrl;

    public static void main(String[] args) {
        SpringApplication.run(ApiRunner.class, args);
    }

    @Bean
    public CommandLineRunner commandLineRunner(ApplicationContext ctx) {

        String gatewayBaseUrl = gatewayBaseProtocol + "://" + gatewayBaseDomain;
        if(StringUtils.isNoneBlank(gatewayBasePort)){
            gatewayBaseUrl += ":" + gatewayBasePort;
        }
        gatewayBaseUrl += "/";
        // init gateway domain
        OkHttpUtils.setBaseUrl(gatewayBaseUrl);
        if(shopBaseUrl!=null&&!"".equals(shopBaseUrl)){
            OkHttpUtils.setShopUrl(shopBaseUrl);
        }

        com.unionpay.acp.sdk.SDKConfig.getConfig().loadPropertiesFromSrc();

        com.unionpay.acp.sdk.SDKConfigForDf.getConfig().loadPropertiesFromSrc();

        com.unionpay.acpverify.sdk.SDKConfig.getConfig().loadPropertiesFromSrc();

        EncryptConverter.getOriginToken("init");

        log.debug("startup!");

        return args -> {
            System.out.println("All beans of wallet-api provided by Spring Boot:");

            String[] beanNames = ctx.getBeanDefinitionNames();
            Arrays.sort(beanNames);
            for (String beanName : beanNames) {
                System.out.println(beanName);
            }
        };
    }
}
