//-----------------------------------------------------------------------------
//      @Author     : GuanJie XiaoJunfeng ShiYunling(Diana SHI)
//      @License    : GNU General Public License, version 3 (GPL-3.0)
//
//      Copyright (C)2018 GuanJie XiaoJunfeng ShiYunling(Diana SHI)
//
//      This program is open source: you can redistribute it and/or modify
//      it under the terms of the GNU General Public License as published by
//      the Free Software Foundation, either version 3 of the License, or
//      (at your option) any later version.
//
//      This program is distributed in the hope that it will be useful,
//      but WITHOUT ANY WARRANTY; without even the implied warranty of
//      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
//      GNU General Public License for more details.
//
//      You should have received a copy of the GNU General Public License
//      along with this program.If not, see < https://www.gnu.org/licenses/>.
//-----------------------------------------------------------------------------
package cn.jointcenter.wallet.api.controller.bank;


import cn.jointcenter.common.util.RestUtil;
import cn.jointcenter.wallet.api.base.BaseController;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@Api(value="Bank Transaction Controller", description="银行交易相关接口: 发行货币，查询发行记录等", tags={"Bank Transaction Controller"})
@RestController
@RequestMapping(value="/api/v1/bank/", produces = {"application/json;charset=UTF-8"})
public class BankTransactionController extends BaseController {

//    private final IssuingService issuingService;
//
//    @Autowired
//    public BankTransactionController(IssuingService issuingService) {
//        this.issuingService = issuingService;
//    }
//
//    @ApiOperation(value="查询发行记录", notes="查询指定银行的数字货币发行记录")
//    @ApiImplicitParams({
//            @ApiImplicitParam(name = "accountId", value = "账户Id", dataType = "Integer", paramType = "path")
//    })
////    @LoginRequired
//    @GetMapping(value="issuing/list/{accountId}")
//    public RestUtil<List<IssuingModel>> getIssuings(@PathVariable Integer accountId){
//        List<IssuingModel> issuingModels = issuingService.findIssuingsByAccount(accountId);
//        return RestUtil.success("", issuingModels);
//    }
//
//    @ApiOperation(value="发行货币", notes="银行发行数字货币")
//    @ApiImplicitParams({
//            @ApiImplicitParam(name = "accountId", value = "账户Id", dataType = "Integer", paramType = "path"),
//            @ApiImplicitParam(name = "amount", value = "金额", dataType = "Double", paramType = "query")
//    })
////    @LoginRequired
//    @GetMapping(value="issuing/start/{accountId}")
//    public RestUtil<Integer> startIssuing(@PathVariable Integer accountId,
//                                                    Double amount){
//        IssuingModel issuingModel = new IssuingModel();
//        issuingModel.setAccountId(accountId);
//        issuingModel.setAmount(amount);
//        issuingModel.setZoneId(1);
//        issuingModel.setOperator("清算系统");
//        return RestUtil.success("", issuingService.createIssuing(issuingModel));
//    }
}
