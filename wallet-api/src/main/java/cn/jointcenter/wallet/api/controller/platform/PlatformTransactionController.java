//-----------------------------------------------------------------------------
//      @Author     : GuanJie XiaoJunfeng ShiYunling(Diana SHI)
//      @License    : GNU General Public License, version 3 (GPL-3.0)
//
//      Copyright (C)2018 GuanJie XiaoJunfeng ShiYunling(Diana SHI)
//
//      This program is open source: you can redistribute it and/or modify
//      it under the terms of the GNU General Public License as published by
//      the Free Software Foundation, either version 3 of the License, or
//      (at your option) any later version.
//
//      This program is distributed in the hope that it will be useful,
//      but WITHOUT ANY WARRANTY; without even the implied warranty of
//      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
//      GNU General Public License for more details.
//
//      You should have received a copy of the GNU General Public License
//      along with this program.If not, see < https://www.gnu.org/licenses/>.
//-----------------------------------------------------------------------------
package cn.jointcenter.wallet.api.controller.platform;


import cn.jointcenter.common.util.RestUtil;
import cn.jointcenter.model.TransactionHistoryModel;
import cn.jointcenter.wallet.api.base.BaseController;
import cn.jointcenter.wallet.service.PayService;
import cn.jointcenter.wallet.service.TransactionHistoryService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@Api(value="Platform Transaction Controller", description="平台交易相关接口: 查询交易记录等", tags={"Platform Transaction Controller"})
@RestController
@RequestMapping(value="/api/v1/platform", produces = {"application/json;charset=UTF-8"})
public class PlatformTransactionController extends BaseController {

    private final TransactionHistoryService transactionService;
    private final PayService payService;

    @Autowired
    public PlatformTransactionController(TransactionHistoryService transactionService,
                                         PayService payService) {
        this.transactionService = transactionService;
        this.payService = payService;
    }

    @GetMapping(value="/latest-pays")
    public RestUtil getLast10Pays(){
        return RestUtil.success("", payService.findLatest10Pays());
    }

//    @ApiOperation(value="查询交易记录", notes="查询指定账号下的交易记录")
////    @LoginRequired
//    @GetMapping(value="trans/list")
//    public RestUtil<List<TransactionHistoryModel>> getTransactions(){
//        List<TransactionHistoryModel> transactionModels = transactionService.findTransactions(0, 0, 0);
//        List<TransactionHistoryModel> newList = new ArrayList<>();
//        if(null != transactionModels) {
//            for (TransactionHistoryModel transactionModel:transactionModels) {
//                if (2 == transactionModel.getState()) {
//                    newList.add(transactionModel);
//                }
//            }
//        }
//        Collections.reverse(newList);
//        return RestUtil.success("", newList);
//    }

}
