//-----------------------------------------------------------------------------
//      @Author     : GuanJie XiaoJunfeng ShiYunling(Diana SHI)
//      @License    : GNU General Public License, version 3 (GPL-3.0)
//
//      Copyright (C)2018 GuanJie XiaoJunfeng ShiYunling(Diana SHI)
//
//      This program is open source: you can redistribute it and/or modify
//      it under the terms of the GNU General Public License as published by
//      the Free Software Foundation, either version 3 of the License, or
//      (at your option) any later version.
//
//      This program is distributed in the hope that it will be useful,
//      but WITHOUT ANY WARRANTY; without even the implied warranty of
//      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
//      GNU General Public License for more details.
//
//      You should have received a copy of the GNU General Public License
//      along with this program.If not, see < https://www.gnu.org/licenses/>.
//-----------------------------------------------------------------------------
package cn.jointcenter.wallet.api.base;

import cn.jointcenter.common.util.RestUtil;

import java.util.HashMap;
import java.util.Map;

public class UnionPayHelper {

    /**
     * validate union card
     * @param cardNo
     * @param userName
     * @param phone
     * @param idNo
     * @return
     */
    public static RestUtil<String> verifyBankCard(String cardNo, String userName, String phone, String idNo){

        String merId = com.unionpay.acpverify.sdk.MerchantUtil.merchant_id;
        String orderId =  com.unionpay.acpverify.sdk.MerchantUtil.getOrderId();
        String txnTime =  com.unionpay.acpverify.sdk.MerchantUtil.getCurrentTime();

        Map<String, String> data = new HashMap<>();

        /***银联全渠道系统，产品参数，除了encoding自行选择外其他不需修改***/
        data.put("version", com.unionpay.acpverify.sdk.MerchantUtil.version);                 //版本号
        data.put("encoding", com.unionpay.acpverify.sdk.MerchantUtil.encoding);               //字符集编码 可以使用UTF-8,GBK两种方式
        data.put("signMethod", com.unionpay.acpverify.sdk.SDKConfig.getConfig().getSignMethod()); //签名方法
        data.put("txnType", "72");                             //交易类型 00-默认
        data.put("txnSubType", "12");                          //交易子类型 银行卡认证
        data.put("bizType", "000803");                         //业务类型 代收

        //B136:银行卡二要素（卡号、姓名 ）
        //B144：银行卡二要素（卡号、证件类型、证件号）
        //B132：银行卡二要素（卡号、手机号）
        //B152：银行卡三要素（卡号、身份证号、姓名）
        //B156：银行卡四要素（卡号、手机号、身份证号、姓名）
        data.put("productId", "B156");


        /***商户接入参数***/
        data.put("merId", merId);                  			   //商户号码，请改成自己申请的商户号或者open上注册得来的777商户号测试
        data.put("accessType", "0");                           //接入类型，商户接入固定填0，不需修改

        /***要调通交易以下字段必须修改***/
        data.put("orderId", orderId);                 //****商户订单号，每次发交易测试需修改为被查询的交易的订单号
        data.put("txnTime", txnTime);                 //****订单发送时间，每次发交易测试需修改为被查询的交易的订单发送时间


        //B152：银行卡三要素（卡号、身份证号、姓名）
        //B156：银行卡四要素（卡号、手机号、身份证号、姓名）
        //根据 productId 的值选择选送类型

        //借记卡可上送姓名、手机号、证件类型、证件号码；
        //贷记卡可上送姓名、手机号、证件类型、证件号码、有效期、CVN2。
        //此测试商户号777290058110097 后台开通业务只支持 贷记卡
        Map<String,String> customerInfoMap = new HashMap<>();
        customerInfoMap.put("customerNm", userName);					//姓名
        customerInfoMap.put("phoneNo", phone);			        //手机号
        customerInfoMap.put("certifTp", "01");						//证件类型
        customerInfoMap.put("certifId", idNo);		//证件号码

        String accNo = cardNo;
        //贷记卡时上送
        //customerInfoMap.put("cvn2", req.getParameter("cvn2"));           			        //卡背面的cvn2三位数字
        //customerInfoMap.put("expired",  req.getParameter("expired"));  				        //有效期 年在前月在后


        ////////////如果商户号开通了【商户对敏感信息加密】的权限那么需要对 accNo，pin和phoneNo，cvn2，expired加密（如果这些上送的话），对敏感信息加密使用：
        String accNo1 = com.unionpay.acpverify.sdk.AcpService.encryptData(accNo, com.unionpay.acpverify.sdk.MerchantUtil.encoding);  //这里测试的时候使用的是测试卡号，正式环境请使用真实卡号
        data.put("accNo", accNo1);
		/*当需要加密敏感信息如CVN2、有效期、密码及其他账户信息如卡号、手机号时，
		填写加密公钥证书的Serial Number*/
        data.put("encryptCertId", com.unionpay.acpverify.sdk.AcpService.getEncryptCertId());       //加密证书的certId，配置在acp_sdk.properties文件 acpsdk.encryptCert.path属性下
        String customerInfoStr = com.unionpay.acpverify.sdk.AcpService.getCustomerInfoWithEncrypt(customerInfoMap,null,com.unionpay.acpverify.sdk.MerchantUtil.encoding);

        data.put("customerInfo", customerInfoStr);

        /**请求参数设置完毕，以下对请求参数进行签名并发送http post请求，接收同步应答报文------------->**/

        Map<String, String> reqData = com.unionpay.acpverify.sdk.AcpService.sign(data, com.unionpay.acpverify.sdk.MerchantUtil.encoding);			//报文中certId,signature的值是在signData方法中获取并自动赋值的，只要证书配置正确即可。
        String url = com.unionpay.acpverify.sdk.SDKConfig.getConfig().getBackRequestUrl();								   //交易请求url从配置文件读取对应属性文件acp_sdk.properties中的 acpsdk.backTransUrl
        Map<String, String> rspData = com.unionpay.acpverify.sdk.AcpService.post(reqData,url, com.unionpay.acpverify.sdk.MerchantUtil.encoding);  //发送请求报文并接受同步应答（默认连接超时时间30秒，读取返回结果超时时间30秒）;这里调用signData之后，调用submitUrl之前不能对submitFromData中的键值对做任何修改，如果修改会导致验签不通过

        /**对应答码的处理，请根据您的业务逻辑来编写程序,以下应答码处理逻辑仅供参考------------->**/
        //应答码规范参考open.unionpay.com帮助中心 下载  产品接口规范  《平台接入接口规范-第5部分-附录》
        if(!rspData.isEmpty()){
            if(com.unionpay.acpverify.sdk.AcpService.validate(rspData, com.unionpay.acpverify.sdk.MerchantUtil.encoding)){
                com.unionpay.acpverify.sdk.LogUtil.writeLog("验证签名成功");
                if(("00").equals(rspData.get("respCode"))){//如果查询交易成功
                    //交易成功，更新商户订单状态
                    return RestUtil.success("", "ok");

                }else if(("34").equals(rspData.get("respCode"))){
                    //订单不存在，可认为交易状态未明，需要稍后发起银行卡认证 ，或依据对账结果为准
                    return RestUtil.error(104, "订单不存在");

                }else{//查询交易本身失败，如应答码10/11检查查询报文是否正确
                    return RestUtil.error(103, "查询失败");
                }
            }else{
                com.unionpay.acpverify.sdk.LogUtil.writeErrorLog("验证签名失败");
                //检查验证签名失败的原因
                return RestUtil.error(102, "验证签名失败");
            }
        }else{
            //未返回正确的http状态
            com.unionpay.acpverify.sdk.LogUtil.writeErrorLog("未获取到返回报文或返回http状态码非200");

            return RestUtil.error(101, "未获取到返回报文或返回http状态码非200");
        }
    }

    /**
     * query card info
     * @param cardNo
     * @return
     */
    public static RestUtil queryCardInfo(String cardNo){
        String merId = com.unionpay.acpverify.sdk.MerchantUtil.merchant_id;
        String orderId = com.unionpay.acpverify.sdk.MerchantUtil.getOrderId();
        String txnTime = com.unionpay.acpverify.sdk.MerchantUtil.getCurrentTime();
        String accNo = cardNo;

        Map<String, String> data = new HashMap<>();

        /***银联全渠道系统，产品参数，除了encoding自行选择外其他不需修改***/
        data.put("version", com.unionpay.acpverify.sdk.MerchantUtil.version);                 //版本号
        data.put("encoding", com.unionpay.acpverify.sdk.MerchantUtil.encoding);               //字符集编码 可以使用UTF-8,GBK两种方式
        data.put("signMethod", com.unionpay.acpverify.sdk.SDKConfig.getConfig().getSignMethod()); //签名方法
        data.put("txnType", "70");                             //交易类型 00-默认
        data.put("txnSubType", "03");                          //交易子类型  银行卡信息查询
        data.put("bizType", "000803");                         //业务类型 代收

        data.put("productId", "Q001");                         //产品ID 银行卡属性和发卡行查询

        /***商户接入参数***/
        data.put("merId", merId);                  			   //商户号码，请改成自己申请的商户号或者open上注册得来的777商户号测试

        /***要调通交易以下字段必须修改***/
        data.put("orderId", orderId);                 //****商户订单号，每次发交易测试需修改为被查询的交易的订单号
        data.put("txnTime", txnTime);                 //****订单发送时间，每次发交易测试需修改为被查询的交易的订单发送时间

        String accNo1 = com.unionpay.acpverify.sdk.AcpService.encryptData(accNo, com.unionpay.acpverify.sdk.MerchantUtil.encoding);   //这里测试的时候使用的是测试卡号，正式环境请使用真实卡号
        data.put("accNo", accNo1);
		/*当需要加密敏感信息如CVN2、有效期、密码及其他账户信息如卡号、手机号时，
		填写加密公钥证书的Serial Number*/
        data.put("encryptCertId", com.unionpay.acpverify.sdk.AcpService.getEncryptCertId());       //加密证书的certId，配置在acp_sdk.properties文件 acpsdk.encryptCert.path属性下

        /**请求参数设置完毕，以下对请求参数进行签名并发送http post请求，接收同步应答报文------------->**/

        Map<String, String> reqData = com.unionpay.acpverify.sdk.AcpService.sign(data,com.unionpay.acpverify.sdk.MerchantUtil.encoding);			//报文中certId,signature的值是在signData方法中获取并自动赋值的，只要证书配置正确即可。
        String url = com.unionpay.acpverify.sdk.SDKConfig.getConfig().getBackRequestUrl();										   //交易请求url从配置文件读取对应属性文件acp_sdk.properties中的 acpsdk.backTransUrl
        Map<String, String> rspData = com.unionpay.acpverify.sdk.AcpService.post(reqData,url, com.unionpay.acpverify.sdk.MerchantUtil.encoding);  //发送请求报文并接受同步应答（默认连接超时时间30秒，读取返回结果超时时间30秒）;这里调用signData之后，调用submitUrl之前不能对submitFromData中的键值对做任何修改，如果修改会导致验签不通过

        /**对应答码的处理，请根据您的业务逻辑来编写程序,以下应答码处理逻辑仅供参考------------->**/
        //应答码规范参考open.unionpay.com帮助中心 下载  产品接口规范  《平台接入接口规范-第5部分-附录》
        if(!rspData.isEmpty()){
            if(com.unionpay.acpverify.sdk.AcpService.validate(rspData, com.unionpay.acpverify.sdk.MerchantUtil.encoding)){
                com.unionpay.acpverify.sdk.LogUtil.writeLog("验证签名成功");
                if(("00").equals(rspData.get("respCode"))){//如果查询交易成功
                    Map<String, String> mapRet = new HashMap<>();
                    mapRet.put("issueAbbr", rspData.get("issueAbbr"));
                    mapRet.put("issueName", rspData.get("issueName"));
                    mapRet.put("issInsCode", rspData.get("issInsCode"));
                    return RestUtil.success("", mapRet);
                }else if(("34").equals(rspData.get("respCode"))){
                    //订单不存在，可认为交易状态未明，需要稍后发起银行卡信息查询，或依据对账结果为准
                    return RestUtil.error(104, "订单不存在");

                }else{//查询交易本身失败，如应答码10/11检查查询报文是否正确
                    return RestUtil.error(103, "查询失败");
                }
            }else{
                com.unionpay.acpverify.sdk.LogUtil.writeErrorLog("验证签名失败");
                return RestUtil.error(102, "验证签名失败");
            }
        }else{
            //未返回正确的http状态
            com.unionpay.acpverify.sdk.LogUtil.writeErrorLog("未获取到返回报文或返回http状态码非200");
            return RestUtil.error(101, "未获取到返回报文或返回http状态码非200");
        }
    }

    /**
     *
     * @param orderId bank transaction id
     * @param idNo id number
     * @param cardNo card no
     * @param amount withdraw amount, unit: fen
     * @return
     */
    public static boolean startDaiFu(String orderId, String idNo, String cardNo, Integer amount){
        //test cardNo "6216261000000000018"
        //TODO test only
        cardNo = "6216261000000000018";
        idNo = "341126197709218366";

        String merId = com.unionpay.acp.sdk.MerchantUtilForDf.merchant_id;
        String txnAmt = String.valueOf(amount);
        String txnTime = com.unionpay.acp.sdk.MerchantUtilForDf.getCurrentTime();

        Map<String, String> data = new HashMap<>();

        /***银联全渠道系统，产品参数，除了encoding自行选择外其他不需修改***/
        data.put("version", com.unionpay.acp.sdk.MerchantUtilForDf.version);            //版本号 全渠道默认值
        data.put("encoding", com.unionpay.acp.sdk.MerchantUtilForDf.encoding);     //字符集编码 可以使用UTF-8,GBK两种方式
        data.put("signMethod", com.unionpay.acp.sdk.SDKConfigForDf.getConfig().getSignMethod()); //签名方法
        data.put("txnType", "12");              		 	//交易类型 12：代付
        data.put("txnSubType", "00");           		 	//默认填写00
        data.put("bizType", "000401");          		 	//000401：代付
        data.put("channelType", "07");          		 	//渠道类型

        /***商户接入参数***/
        data.put("merId", merId);   		 				//商户号码，请改成自己申请的商户号或者open上注册得来的777商户号测试
        data.put("accessType", "0");            		 	//接入类型，商户接入填0 ，不需修改（0：直连商户， 1： 收单机构 2：平台商户）
        data.put("orderId", orderId);        	 	    	//商户订单号，8-40位数字字母，不能含“-”或“_”，可以自行定制规则
        data.put("txnTime", txnTime);		 		    	//订单发送时间，取系统时间，格式为YYYYMMDDhhmmss，必须取当前时间，否则会报txnTime无效
        data.put("accType", "01");					 		//账号类型 01：银行卡

        //sourcesOfFunds为01时payerVerifiInfo必送，其他情况不送payerVerifiInfo。
        //付款方账号        payerAccNo     1到19位数字
        //付款方姓名        payerNm 30字节以下，支持汉字，1个汉字算2字节
        //data.put("sourcesOfFunds", "01");
        //data.put("payerVerifiInfo", "{payerAccNo=6226090000000048&payerNm=张三}");


        //收款账号为对公时：测试卡使用 6212142600000000167（单位结算卡）
        //单位结算卡完整账户名称        comDebitCardAccName 120字节以下，支持汉字，1个汉字算2字节
        //营业执照注册号        businessLicenseRegNo 20字节以下，支持汉字，1个汉字算2字节
        //data.put("accType", "04"); //04表示对公账户,当04时不需要送customerInfo
        //data.put("reserved", "{comDebitCardAccName=中国银联单位结算卡&businessLicenseRegNo=1101888888}");

        //////////如果商户号开通了  商户对敏感信息加密的权限那么，需要对 卡号accNo加密使用：
        data.put("encryptCertId",com.unionpay.acp.sdk.AcpService.getEncryptCertId());      						//上送敏感信息加密域的加密证书序列号
        String accNo = com.unionpay.acp.sdk.AcpService.encryptData(cardNo, com.unionpay.acp.sdk.MerchantUtilForDf.encoding); 	//这里测试的时候使用的是测试卡号，正式环境请使用真实卡号
        data.put("accNo", accNo);
        //////////

        /////////商户未开通敏感信息加密的权限那么不对敏感信息加密使用：
        //contentData.put("accNo", "6216261000000000018");                  				//这里测试的时候使用的是测试卡号，正式环境请使用真实卡号
        ////////

        //代付交易的上送的卡验证要素：姓名或者证件类型+证件号码
        Map<String,String> customerInfoMap = new HashMap<String,String>();
        customerInfoMap.put("certifTp", "01");						    //证件类型
        customerInfoMap.put("certifId", idNo);		    //证件号码
        //customerInfoMap.put("customerNm", "全渠道");					//姓名
        String customerInfoStr = com.unionpay.acp.sdk.AcpService.getCustomerInfo(customerInfoMap,cardNo,com.unionpay.acp.sdk.MerchantUtilForDf.encoding);

        data.put("customerInfo", customerInfoStr);
        data.put("txnAmt", txnAmt);						 		    //交易金额 单位为分，不能带小数点
        data.put("currencyCode", "156");                    	    //境内商户固定 156 人民币
        data.put("billNo", "存钱");                    	            //银行附言。会透传给发卡行，完成改造的发卡行会把这个信息在账单、短信中显示给用户的，请按真实情况填写。


        //后台通知地址（需设置为外网能访问 http https均可），支付成功后银联会自动将异步通知报文post到商户上送的该地址，【支付失败的交易银联不会发送后台通知】
        //后台通知参数详见open.unionpay.com帮助中心 下载  产品接口规范  网关支付产品接口规范 消费交易 商户通知
        //注意:1.需设置为外网能访问，否则收不到通知    2.http https均可  3.收单后台通知后需要10秒内返回http200或302状态码
        //    4.如果银联通知服务器发送通知后10秒内未收到返回状态码或者应答码非http200或302，那么银联会间隔一段时间再次发送。总共发送5次，银联后续间隔1、2、4、5 分钟后会再次通知。
        //    5.后台通知地址如果上送了带有？的参数，例如：http://abc/web?a=b&c=d 在后台通知处理程序验证签名之前需要编写逻辑将这些字段去掉再验签，否则将会验签失败
        data.put("backUrl", com.unionpay.acp.sdk.MerchantUtilForDf.backUrl);

        // 请求方保留域，
        // 透传字段，查询、通知、对账文件中均会原样出现，如有需要请启用并修改自己希望透传的数据。
        // 出现部分特殊字符时可能影响解析，请按下面建议的方式填写：
        // 1. 如果能确定内容不会出现&={}[]"'等符号时，可以直接填写数据，建议的方法如下。
//		data.put("reqReserved", "透传信息1|透传信息2|透传信息3");
        // 2. 内容可能出现&={}[]"'符号时：
        // 1) 如果需要对账文件里能显示，可将字符替换成全角＆＝｛｝【】“‘字符（自己写代码，此处不演示）；
        // 2) 如果对账文件没有显示要求，可做一下base64（如下）。
        //    注意控制数据长度，实际传输的数据长度不能超过1024位。
        //    查询、通知等接口解析时使用new String(Base64.decodeBase64(reqReserved), DemoBase.encoding);解base64后再对数据做后续解析。
//		data.put("reqReserved", Base64.encodeBase64String("任意格式的信息都可以".toString().getBytes(DemoBase.encoding)));


        /**对请求参数进行签名并发送http post请求，接收同步应答报文**/
        Map<String, String> reqData = com.unionpay.acp.sdk.AcpService.sign(data,com.unionpay.acp.sdk.MerchantUtilForDf.encoding);			 		 //报文中certId,signature的值是在signData方法中获取并自动赋值的，只要证书配置正确即可。
        String requestBackUrl = com.unionpay.acp.sdk.SDKConfigForDf.getConfig().getBackRequestUrl();									 //交易请求url从配置文件读取对应属性文件acp_sdk.properties中的 acpsdk.backTransUrl

        Map<String, String> rspData = com.unionpay.acp.sdk.AcpService.post(reqData,requestBackUrl,com.unionpay.acp.sdk.MerchantUtilForDf.encoding);        //发送请求报文并接受同步应答（默认连接超时时间30秒，读取返回结果超时时间30秒）;这里调用signData之后，调用submitUrl之前不能对submitFromData中的键值对做任何修改，如果修改会导致验签不通过
        /**对应答码的处理，请根据您的业务逻辑来编写程序,以下应答码处理逻辑仅供参考------------->**/
        //应答码规范参考open.unionpay.com帮助中心 下载  产品接口规范  《平台接入接口规范-第5部分-附录》
        if(!rspData.isEmpty()){
            if(com.unionpay.acp.sdk.AcpService.validate(rspData, com.unionpay.acp.sdk.MerchantUtilForDf.encoding)){
                com.unionpay.acp.sdk.LogUtil.writeLog("验证签名成功");
                String respCode = rspData.get("respCode");
                if(("00").equals(respCode)){
                    //交易已受理(不代表交易已成功），等待接收后台通知确定交易成功，也可以主动发起 查询交易确定交易状态。
                    //TODO
                    return true;
                    //如果返回卡号且配置了敏感信息加密，解密卡号方法：
                    //String accNo1 = resmap.get("accNo");
                    //String accNo2 = AcpService.decryptPan(accNo1, "UTF-8");	//解密卡号使用的证书是商户签名私钥证书acpsdk.signCert.path
                    //LogUtil.writeLog("解密后的卡号："+accNo2);
                }else if(("03").equals(respCode) ||
                        ("04").equals(respCode) ||
                        ("05").equals(respCode) ||
                        ("01").equals(respCode) ||
                        ("12").equals(respCode) ||
                        ("60").equals(respCode) ){
                    //后续需发起交易状态查询交易确定交易状态。
                    //TODO
                }else{
                    //其他应答码为失败请排查原因
                    //TODO
                }
            }else{
                com.unionpay.acp.sdk.LogUtil.writeErrorLog("验证签名失败");
                //TODO 检查验证签名失败的原因
            }
        }else{
            //未返回正确的http状态
            com.unionpay.acp.sdk.LogUtil.writeErrorLog("未获取到返回报文或返回http状态码非200");
        }
        return false;
    }
}
