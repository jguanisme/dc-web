//-----------------------------------------------------------------------------
//      @Author     : GuanJie XiaoJunfeng ShiYunling(Diana SHI)
//      @License    : GNU General Public License, version 3 (GPL-3.0)
//
//      Copyright (C)2018 GuanJie XiaoJunfeng ShiYunling(Diana SHI)
//
//      This program is open source: you can redistribute it and/or modify
//      it under the terms of the GNU General Public License as published by
//      the Free Software Foundation, either version 3 of the License, or
//      (at your option) any later version.
//
//      This program is distributed in the hope that it will be useful,
//      but WITHOUT ANY WARRANTY; without even the implied warranty of
//      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
//      GNU General Public License for more details.
//
//      You should have received a copy of the GNU General Public License
//      along with this program.If not, see < https://www.gnu.org/licenses/>.
//-----------------------------------------------------------------------------
package cn.jointcenter.wallet.api.controller.user;


import cn.jointcenter.account.service.*;
import cn.jointcenter.common.annotation.LoginRequired;
import cn.jointcenter.common.exception.GatewayException;
import cn.jointcenter.common.util.JsonUtil;
import cn.jointcenter.common.util.RestUtil;
import cn.jointcenter.model.*;
import cn.jointcenter.wallet.api.base.BaseController;
import cn.jointcenter.wallet.api.base.RestCode;
import cn.jointcenter.wallet.api.base.UnionPayHelper;
import cn.jointcenter.wallet.service.BankCardService;
import cn.jointcenter.wallet.service.CurrencyService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestClientException;

import java.util.*;

@Api(value="User Account Controller", description="用户钱包的基本信息、设置接口", tags={"User Account Controller"})
@RestController
@RequestMapping(value="/api/v1/user/", produces = {"application/json;charset=UTF-8"})
public class UserAccountController extends BaseController {

    private final UserService userService;
    private final AccountService accountService;
    private final WalletService walletService;
    private final ShopService shopService;
    private final CurrencyService currencyService;
    private final BankCardService bankCardService;
    private final RedisTemplate<String, String> redisTemplate;

    @Autowired
    public UserAccountController(UserService userService, ShopService shopService,
                                 AccountService accountService, WalletService walletService,
                                 CurrencyService currencyService,
                                 BankCardService bankCardService,
                                 RedisTemplate<String, String> redisTemplate) {
        this.userService = userService;
        this.accountService = accountService;
        this.walletService = walletService;
        this.shopService = shopService;
        this.currencyService = currencyService;
        this.bankCardService = bankCardService;
        this.redisTemplate = redisTemplate;
    }

    @ApiOperation(value="钱包首页", notes="钱包首页，显示账户列表")
    @LoginRequired
    @GetMapping(value="wallet")
    public RestUtil walletIndex(){
        String utxo = this.getUTXO();

        Map<String, Object> map = new HashMap<>();
        WalletModel walletModel = walletService.getWalletByUtxo(utxo);
        map.put("amount", walletModel.getBalance());

        List<CardModel> cardModels = bankCardService.getBankCardsByWallet(walletModel.getId());
        map.put("cards", cardModels);
        map.put("address", utxo);

        return RestUtil.success("", map);
    }

    @ApiOperation(value="个人中心数据", notes="个人中心数据，昵称(nickname)/手机号(mobile)/姓名(trueName)/身份证(idNumber)")
    @LoginRequired
    @GetMapping(value="profile")
    public RestUtil profileIndex(){
        String utxo = this.getUTXO();

        Map<String, Object> map = new HashMap<>();
        WalletModel walletModel = walletService.getWalletByUtxo(utxo);
        map.put("nickname", walletModel.getName());

        List<CardModel> cardModels = bankCardService.getBankCardsByWallet(walletModel.getId());
        if(null == cardModels || cardModels.isEmpty()){
            map.put("trueName", "");
            map.put("idNumber", "");
        }
        else{
            map.put("trueName", cardModels.get(0).getPersonName());
            map.put("idNumber", cardModels.get(0).getIdNumber());
        }

        AccountModel account = accountService.getAccount(walletModel.getAccountId());
        String loginName = account.getName();
        if(StringUtils.isNoneBlank(loginName) && loginName.length() == 11){
            map.put("mobile", loginName);
        }
        else{
            map.put("mobile", "");
        }

        return RestUtil.success("", map);
    }

    @ApiOperation(value="找回钱包", notes="找回钱包，返回UTXO与公钥")
    @PostMapping(value="find-wallet")
    public RestUtil findMyWallet(String mobile, String captcha, String pwd, String idNumber){
        if(StringUtils.isBlank(mobile) || StringUtils.isBlank(captcha)){
            return restFail(RestCode.PARAM_ERROR);
        }

        RestUtil rest = this.checkMobile(mobile);
        if(1!=rest.getCode()){
            return rest;
        }

        //check captcha
        String savedCaptcha = redisTemplate.opsForValue().get(CAPTCHA_PREFIX + mobile);
        if(!captcha.equals(savedCaptcha)){
            return restFail(RestCode.SMS_CAPTCHA_WRONG);
        }

        AccountModel account = accountService.getByName(mobile);
        if(null == account){
            return restFail(RestCode.NO_SUCH_USER);
        }

        //compare with salt
//        String calcPwd = securityUtil.encodePwd(pwd, account.getSalt());
//        if(!calcPwd.equals(account.getLoginPwd())){
//            return  restFail(RestCode.INCORRECT_PWD);
//        }

        WalletModel walletModel = walletService.getWalletByAccount(account.getId());
        if(null == walletModel){
            return restFail(RestCode.WALLET_EMPTY);
        }
        String utxo = walletModel.getUtxoAddress();

//        List<CardModel> cardModels = bankCardService.getBankCardsByWallet(walletModel.getId());
//        if(null == cardModels || cardModels.isEmpty()){
//            return restFail(RestCode.NO_ID_AUTH);
//        }
//        if(!idNumber.equals(cardModels.get(0).getIdNumber())){
//            return restFail(RestCode.ID_AUTH_NOT_MATCH);
//        }
        Map<String, Object> map = new HashMap<>();
        map.put("pub", walletModel.getPubKey());
        map.put("address", utxo);

        return RestUtil.success("", map);
    }

//    @ApiOperation(value="钱包下的货币列表", notes="查询用户的一个钱包下的货币列表")
//    @ApiImplicitParam(name="walletId", value="账户Id", dataType="Integer", paramType = "path")
//    @LoginRequired
//    @GetMapping(value="{walletId}/currencies")
//    public RestUtil<List<CurrencyModel>> getCurrenciesByAccount(@PathVariable("walletId") Integer accountId){
//        List<CurrencyModel> currencyModels = currencyService.(accountId, 0);
//
//        if(null != currencyModels) {
//            for (CurrencyModel currencyModel : currencyModels) {
//                currencyModel.setCreated(this.formatDateTimeWithZone(currencyModel.getCreated()));
//            }
//        }
//        return RestUtil.success("", currencyModels);
//    }
//
//    @ApiOperation(value="用户的账户列表", notes="用户钱包里所拥有的账户列表，不包含余额")
//    @LoginRequired
//    @GetMapping(value="accounts")
//    public RestUtil<List<AccountModel>> getWalletAccounts(){
//        List<AccountModel> accountModelList = accountService.findAccounts(this.getUserId());
//        return RestUtil.success("", accountModelList);
//    }
//
//    @ApiOperation(value="账户的状态与金额", notes="查询用户的一个账户的状态与金额")
//    @ApiImplicitParam(name="accountId", value="账户Id", dataType="Integer", paramType = "path")
//    @LoginRequired
//    @GetMapping(value="account/{accountId}/state")
//    public RestUtil<AccountStateModel> getAccountState(@PathVariable("accountId") Integer accountId){
//        List<AccountStateModel> accountStateModels = accountStateService.findAccountStates(accountId);
//        if(accountStateModels == null || accountStateModels.size() == 0){
//            return RestUtil.success("状态信息不存在", null);
//        }
//        return RestUtil.success("", accountStateModels.get(0));
//    }

//    @ApiOperation(value="账户的基本信息", notes="查询用户的一个账户的基本信息")
//    @ApiImplicitParam(name="accountId", value="账户Id", dataType="Integer", paramType = "path")
//    @LoginRequired
//    @GetMapping(value="account/{accountId}/info")
//    public RestUtil<AccountModel> getAccountInfo(@PathVariable("accountId") Integer accountId){
//        AccountModel accountModel = accountService.getAccount(accountId);
//        return RestUtil.success("", accountModel);
//    }

    @ApiOperation(value="我的银行卡", notes="获取用户所有的银行卡")
    @LoginRequired
    @GetMapping(value="bankcards")
    public RestUtil<List<CardModel>> getBankCards(){
        Integer walletId = this.getWalletId();
        List<CardModel> bankCards = bankCardService.getBankCardsByWallet(walletId);
        return RestUtil.success("", bankCards);
    }

    @ApiOperation(value="添加银行卡", notes="添加一个银行卡")
    @ApiImplicitParams({
            @ApiImplicitParam(name="userName", value="姓名", dataType="String", paramType = "query"),
            @ApiImplicitParam(name="id", value="身份证号", dataType="String", paramType = "query"),
            @ApiImplicitParam(name="cardNo", value="银行卡号", dataType="String", paramType = "query"),
            @ApiImplicitParam(name="phone", value="预留手机号", dataType="String", paramType = "query")
//            @ApiImplicitParam(name="captcha", value="验证码", dataType="String", paramType = "query")
    })
    @LoginRequired
    @PostMapping(value="bankcard/add")
    public RestUtil<CardModel> addBankCard(String userName, String id, String cardNo,
                                                      String phone) throws RuntimeException {
        Map<String, String> map = new HashMap<>();
        map.put("Name", userName);
        map.put("Id", id);
        map.put("CardNo", cardNo);
        map.put("Phone", phone);

        boolean needUpdateMobile = false;
        //先校验跟原来的银行卡是否同一个实名
        List<CardModel> cardModels = bankCardService.getBankCardsByWallet(this.getWalletId());
        if(null != cardModels && !cardModels.isEmpty()){
            if(!id.equals(cardModels.get(0).getIdNumber())){
                throw new RuntimeException(RestCode.ID_AUTH_NOT_MATCH.getMessage());
            }
            if(!userName.equals(cardModels.get(0).getPersonName())){
                throw new RuntimeException(RestCode.NAME_AUTH_NOT_MATCH.getMessage());
            }

            //验证一下是否存在该卡
            if(cardModels.stream().anyMatch(item -> cardNo.equals(item.getCardNumber()))){
                throw new RuntimeException(RestCode.BANK_CARD_EXIST.getMessage());
            }
        }
        else{
            needUpdateMobile = true;
        }
        //银行卡验证
        RestUtil<String> restValid = UnionPayHelper.verifyBankCard(cardNo, userName, phone, id);
        if(restValid.getCode() != 1){
            throw new RuntimeException("银行卡验证失败");
        }
        RestUtil restInfo = UnionPayHelper.queryCardInfo(cardNo);
        if(restInfo.getCode() != 1){
            throw new RuntimeException("银行卡信息查询失败");
        }

//        if(restInfo.getResult() instanceof Map) {
        Map mapInfo = (Map) restInfo.getResult();
        map.put("IssueAbbr", (String)mapInfo.get("issueAbbr"));
        map.put("IssueName", (String)mapInfo.get("issueName"));
        map.put("IssInsCode", (String)mapInfo.get("issInsCode"));
//        }

        CardModel bankCardModel = new CardModel();
        bankCardModel.setAttribute(JsonUtil.toJson(map));
        bankCardModel.setWalletId(this.getWalletId());
        bankCardModel.setName(userName);
        bankCardModel.setPersonName(userName);
        bankCardModel.setCardNumber(cardNo);
        bankCardModel.setIdNumber(id);
        bankCardModel.setPhone(phone);

        Integer modelId = bankCardService.createBankCard(bankCardModel);
        if(modelId<=0){
            throw new RuntimeException("保存失败");
        }
        bankCardModel.setId(modelId);

//        if(needUpdateMobile){
//            //save mobile
//            WalletModel walletModel = walletService.getWalletByUtxo(this.getUTXO());
//            AccountModel accountModel = accountService.getAccount(walletModel.getAccountId());
//            if(StringUtils.isBlank(accountModel.getName()) || accountModel.getName().length()!=11){
//                AccountModel accountUpdate = new AccountModel();
//                accountUpdate.setId(accountModel.getId());
//                accountUpdate.setName(phone);
//
//                accountService.updateAccount(accountUpdate);
//            }
//        }

        return RestUtil.success("", bankCardModel);
    }

    @ApiOperation(value="设置钱包昵称", notes="设置钱包昵称")
    @ApiImplicitParams({
            @ApiImplicitParam(name="nickname", value="昵称", dataType="String", paramType = "query")
    })
    @LoginRequired
    @PostMapping(value="set-nick")
    public RestUtil addNickname(String nickname) throws RuntimeException {
        if(StringUtils.isBlank(nickname)){
            return restFail(RestCode.PARAM_ERROR);
        }
        String utxo = this.getUTXO();

        WalletModel walletModel = walletService.getWalletByUtxo(utxo);
        if(null == walletModel){
            return restFail(RestCode.WALLET_EMPTY);
        }
        WalletModel walletUpdate = new WalletModel();
        walletUpdate.setId(walletModel.getId());
        walletUpdate.setName(nickname);
        WalletModel saved = walletService.updateWallet(walletUpdate);
        if(null == saved){
            return restFail(RestCode.STORE_ERROR);
        }
        return RestUtil.success("", "");
    }

    @ApiOperation(value="设置手机号", notes="设置手机号")
    @ApiImplicitParams({
            @ApiImplicitParam(name="mobile", value="手机号", dataType="String", paramType = "query"),
            @ApiImplicitParam(name="captcha", value="短信验证码", dataType="String", paramType = "query")
    })
    @LoginRequired
    @PostMapping(value="set-mobile")
    public RestUtil addMobile(String mobile, String captcha) throws RuntimeException {
        if(StringUtils.isBlank(mobile) || StringUtils.isBlank(captcha)){
            return restFail(RestCode.PARAM_ERROR);
        }

        RestUtil rest = this.checkMobile(mobile);
        if(1!=rest.getCode()){
            return rest;
        }

        //check captcha
        String savedCaptcha = redisTemplate.opsForValue().get(CAPTCHA_PREFIX + mobile);
        if(!captcha.equals(savedCaptcha)){
            return restFail(RestCode.SMS_CAPTCHA_WRONG);
        }

        String utxo = this.getUTXO();

        WalletModel walletModel = walletService.getWalletByUtxo(utxo);
        if(null == walletModel){
            return restFail(RestCode.WALLET_EMPTY);
        }
        AccountModel account = new AccountModel();
        account.setId(walletModel.getAccountId());
        account.setName(mobile);
        try {
            AccountModel saved = accountService.updateAccount(account);
            if (null == saved) {
                return restFail(RestCode.ACCOUNT_EMPTY);
            }
        }catch (GatewayException ge){
            if(ge.getCode() == 605){
                return RestUtil.error(ge.getCode(), "手机号已绑定其它钱包");
            }
            return  RestUtil.error(ge.getCode(), ge.getMessage());
        }

        return RestUtil.success("", "");
    }

    @ApiOperation(value="设置密码", notes="设置密码")
    @ApiImplicitParams({
            @ApiImplicitParam(name="pwd", value="新密码", dataType="String", paramType = "query"),
            @ApiImplicitParam(name="oldPwd", value="原密码", dataType="String", paramType = "query")
    })
    @LoginRequired
    @PostMapping(value="set-pwd")
    public RestUtil addPwd(String pwd, String oldPwd) throws RuntimeException {
        if(StringUtils.isBlank(pwd)){
            return restFail(RestCode.PARAM_ERROR);
        }
        String utxo = this.getUTXO();
        WalletModel walletModel = walletService.getWalletByUtxo(utxo);
        if(null == walletModel){
            return restFail(RestCode.WALLET_EMPTY);
        }
        AccountModel account = accountService.getAccount(walletModel.getAccountId());
        String dbPwd = account.getLoginPwd();
        String salt = account.getSalt();
//        String calcPwd = securityUtil.encodePwd(password, salt);

        AccountModel accountUpdate = new AccountModel();
        accountUpdate.setId(walletModel.getAccountId());
        if(StringUtils.isBlank(dbPwd)){
            salt = securityUtil.generalSalt();
            accountUpdate.setSalt(salt);
        }
        else{
            if(StringUtils.isBlank(oldPwd)){
                return restFail(RestCode.PARAM_ERROR);
            }
            if(!dbPwd.equals(securityUtil.encodePwd(oldPwd, salt))){
                return restFail(RestCode.OLD_PWD_ERROR);
            }
        }
        accountUpdate.setLoginPwd(securityUtil.encodePwd(pwd, salt));
        AccountModel saved = accountService.updateAccount(accountUpdate);
        if(null == saved){
            return restFail(RestCode.STORE_ERROR);
        }

        return RestUtil.success("", "");
    }

    @ApiOperation(value="设置支付密码", notes="设置支付密码")
    @ApiImplicitParams({
            @ApiImplicitParam(name="pwd", value="新密码", dataType="String", paramType = "query"),
            @ApiImplicitParam(name="oldPwd", value="原密码", dataType="String", paramType = "query")
    })
    @LoginRequired
    @PostMapping(value="set-paypwd")
    public RestUtil addPayPwd(String pwd, String oldPwd) throws RuntimeException {
        if(StringUtils.isBlank(pwd)){
            return restFail(RestCode.PARAM_ERROR);
        }
        String utxo = this.getUTXO();
        WalletModel walletModel = walletService.getWalletByUtxo(utxo);
        if(null == walletModel){
            return restFail(RestCode.WALLET_EMPTY);
        }
        AccountModel account = accountService.getAccount(walletModel.getAccountId());
        String dbPwd = account.getTradePwd();
//        String calcPwd = securityUtil.encodePwd(password, salt);

        AccountModel accountUpdate = new AccountModel();
        accountUpdate.setId(walletModel.getAccountId());
        if(!StringUtils.isBlank(dbPwd)){
            if(StringUtils.isBlank(oldPwd)){
                return restFail(RestCode.PARAM_ERROR);
            }
            if(!dbPwd.equals(securityUtil.encodePwd(oldPwd, ""))){
                return restFail(RestCode.OLD_PWD_ERROR);
            }
        }
        accountUpdate.setTradePwd(securityUtil.encodePwd(pwd, ""));
        AccountModel saved = accountService.updateAccount(accountUpdate);
        if(null == saved){
            return restFail(RestCode.STORE_ERROR);
        }

        return RestUtil.success("", "");
    }

    @ApiOperation(value="重新设置密码", notes="重新设置密码")
    @ApiImplicitParams({
            @ApiImplicitParam(name="mobile", value="手机", dataType="String", paramType = "query"),
            @ApiImplicitParam(name="captcha", value="短信验证码", dataType="String", paramType = "query"),
            @ApiImplicitParam(name="pwd", value="新密码", dataType="String", paramType = "query")
    })
    @PostMapping(value="reset-pwd")
    public RestUtil resetPwd(String mobile, String captcha, String pwd) throws RuntimeException {
        if(StringUtils.isBlank(mobile) || StringUtils.isBlank(captcha) || StringUtils.isBlank(pwd)){
            return restFail(RestCode.PARAM_ERROR);
        }

        RestUtil rest = this.checkMobile(mobile);
        if(1!=rest.getCode()){
            return rest;
        }

        AccountModel account = accountService.getByName(mobile);
        if(null == account){
            return restFail(RestCode.NO_SUCH_USER);
        }

        //check captcha
        String savedCaptcha = redisTemplate.opsForValue().get(CAPTCHA_PREFIX + mobile);
        if(!captcha.equals(savedCaptcha)){
            return restFail(RestCode.SMS_CAPTCHA_WRONG);
        }

        String salt = account.getSalt();

        AccountModel accountUpdate = new AccountModel();
        accountUpdate.setId(account.getId());
        accountUpdate.setLoginPwd(securityUtil.encodePwd(pwd, salt));
        AccountModel saved = accountService.updateAccount(accountUpdate);
        if(null == saved){
            return restFail(RestCode.STORE_ERROR);
        }

        return RestUtil.success("", "");
    }

    @ApiOperation(value="重新设置支付密码", notes="重新设置支付密码")
    @ApiImplicitParams({
            @ApiImplicitParam(name="mobile", value="手机", dataType="String", paramType = "query"),
            @ApiImplicitParam(name="captcha", value="短信验证码", dataType="String", paramType = "query"),
            @ApiImplicitParam(name="pwd", value="新密码", dataType="String", paramType = "query")
    })
    @LoginRequired
    @PostMapping(value="reset-paypwd")
    public RestUtil resetPayPwd(String mobile, String captcha, String pwd) throws RuntimeException {
        if(StringUtils.isBlank(mobile) || StringUtils.isBlank(captcha) || StringUtils.isBlank(pwd)){
            return restFail(RestCode.PARAM_ERROR);
        }

        RestUtil rest = this.checkMobile(mobile);
        if(1!=rest.getCode()){
            return rest;
        }

        String utxo = this.getUTXO();
        WalletModel walletModel = walletService.getWalletByUtxo(utxo);
        if(null == walletModel){
            return restFail(RestCode.WALLET_EMPTY);
        }
        AccountModel account = accountService.getAccount(walletModel.getAccountId());

        if(!mobile.equals(account.getName())){
            return  restFail(RestCode.MOBILE_NOT_MATCH);
        }

        //check captcha
        String savedCaptcha = redisTemplate.opsForValue().get(CAPTCHA_PREFIX + mobile);
        if(!captcha.equals(savedCaptcha)){
            return restFail(RestCode.SMS_CAPTCHA_WRONG);
        }

        AccountModel accountUpdate = new AccountModel();
        accountUpdate.setId(walletModel.getAccountId());
        accountUpdate.setTradePwd(securityUtil.encodePwd(pwd, ""));
        AccountModel saved = accountService.updateAccount(accountUpdate);
        if(null == saved){
            return restFail(RestCode.STORE_ERROR);
        }

        return RestUtil.success("", "");
    }

}
