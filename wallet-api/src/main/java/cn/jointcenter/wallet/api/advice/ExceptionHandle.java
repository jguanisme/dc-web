//-----------------------------------------------------------------------------
//      @Author     : GuanJie XiaoJunfeng ShiYunling(Diana SHI)
//      @License    : GNU General Public License, version 3 (GPL-3.0)
//
//      Copyright (C)2018 GuanJie XiaoJunfeng ShiYunling(Diana SHI)
//
//      This program is open source: you can redistribute it and/or modify
//      it under the terms of the GNU General Public License as published by
//      the Free Software Foundation, either version 3 of the License, or
//      (at your option) any later version.
//
//      This program is distributed in the hope that it will be useful,
//      but WITHOUT ANY WARRANTY; without even the implied warranty of
//      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
//      GNU General Public License for more details.
//
//      You should have received a copy of the GNU General Public License
//      along with this program.If not, see < https://www.gnu.org/licenses/>.
//-----------------------------------------------------------------------------
package cn.jointcenter.wallet.api.advice;

import cn.jointcenter.common.exception.LoginRequiredException;
import cn.jointcenter.common.exception.OkHttpRuntimeException;
import cn.jointcenter.common.util.RestUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@RestControllerAdvice
@Slf4j
public class ExceptionHandle {

    @ExceptionHandler(value = Exception.class)
    public RestUtil<String> handle(Exception e) {
        String message = e.getMessage();

        if(!StringUtils.isEmpty(message) &&
                ("No enought balance".equals(message) ||
                "No Currency".equals(message))){
            message = "余额不足";
        }

        if (e instanceof OkHttpRuntimeException) {
            log.error("Api Call Error : {}", e);
            return RestUtil.error(500, message);
        }
        if (e instanceof LoginRequiredException) {
            log.error("Known Error : {}", e);
            return RestUtil.error(((LoginRequiredException) e).getCode(), message);
        }
        if (e instanceof RuntimeException){
            log.error("Runtime Error : {}", e);
            return RestUtil.error(500, message);
        }

        log.error("Other Error : {}", e);
        return RestUtil.error(509, message);
    }

}
