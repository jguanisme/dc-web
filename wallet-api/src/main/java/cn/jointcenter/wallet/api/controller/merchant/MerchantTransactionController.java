//-----------------------------------------------------------------------------
//      @Author     : GuanJie XiaoJunfeng ShiYunling(Diana SHI)
//      @License    : GNU General Public License, version 3 (GPL-3.0)
//
//      Copyright (C)2018 GuanJie XiaoJunfeng ShiYunling(Diana SHI)
//
//      This program is open source: you can redistribute it and/or modify
//      it under the terms of the GNU General Public License as published by
//      the Free Software Foundation, either version 3 of the License, or
//      (at your option) any later version.
//
//      This program is distributed in the hope that it will be useful,
//      but WITHOUT ANY WARRANTY; without even the implied warranty of
//      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
//      GNU General Public License for more details.
//
//      You should have received a copy of the GNU General Public License
//      along with this program.If not, see < https://www.gnu.org/licenses/>.
//-----------------------------------------------------------------------------
package cn.jointcenter.wallet.api.controller.merchant;


import cn.jointcenter.account.service.DeviceService;
import cn.jointcenter.account.service.WalletService;
import cn.jointcenter.common.util.PosRequestUtil;
import cn.jointcenter.common.util.PosRestUtil;
import cn.jointcenter.common.util.RestUtil;
import cn.jointcenter.model.DeviceModel;
import cn.jointcenter.model.PayModel;
import cn.jointcenter.model.TransactionHistoryModel;
import cn.jointcenter.model.WalletModel;
import cn.jointcenter.model.merchant.CheckDiscountPayModel;
import cn.jointcenter.model.merchant.PosTransQueryModel;
import cn.jointcenter.model.merchant.ReceiptModel;
import cn.jointcenter.wallet.api.base.BaseController;
import cn.jointcenter.wallet.service.PayService;
import cn.jointcenter.wallet.service.TransactionHistoryService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Slf4j
@Api(value="Merchant Transaction Controller", description="商户交易相关接口: 发起交易、查询交易记录等", tags={"Merchant Transaction Controller"})
@RestController
@RequestMapping(value="/api/v1/merchant/", produces = {"application/json;charset=UTF-8"})
public class MerchantTransactionController extends BaseController {

    private final TransactionHistoryService transactionService;
    private final WalletService walletService;
    private final DeviceService deviceService;
    private final PayService payService;

    @Autowired
    public MerchantTransactionController(TransactionHistoryService transactionService,
                                         WalletService walletService, DeviceService deviceService,
                                         PayService payService) {
        this.transactionService = transactionService;
        this.walletService = walletService;
        this.deviceService = deviceService;
        this.payService = payService;
    }

    private String getWalletAddress(String deviceId){
        //find utxo by deviceId
        DeviceModel deviceModel = deviceService.getDeviceBySN(deviceId);
        if(null == deviceModel){
            throw new RuntimeException("POS设备未注册");
        }
        WalletModel walletModel = walletService.getWalletByAccount(deviceModel.getAccountId());
        if(null == walletModel){
            throw new RuntimeException("POS设备未绑定到商户钱包");
        }
        return walletModel.getUtxoAddress();
    }

    @ApiOperation(value="POS机收款码", notes="POS机收款码")
    @PostMapping(value="pos/my-wallet-addr")
    public PosRestUtil myWalletAddress(@RequestBody @ApiParam(name="POS获取收款码对象", value = "传入json格式") PosRequestUtil<Map<String,Object>> req) {
        try {
            Map<String, Object> map = req.getData();

            String deviceId = (String) map.get("deviceId");
//            String address = getWalletAddress(deviceId);
            Integer amount = (Integer) map.get("amount");

            return PosRestUtil.success("", "pos,"+deviceId+","+amount.toString(), req.getSignature());
        }
        catch (RuntimeException re){
            return PosRestUtil.error(102, re.getMessage(), req.getSignature());
        }
    }

    @ApiOperation(value="发起收款交易", notes="通过POS机发起收款交易")
    @PostMapping(value="pos/start")
    public PosRestUtil startPosTransaction(@RequestBody @ApiParam(name="POS发起交易对象", value = "传入json格式") PosRequestUtil<Map<String,Object>> req){
        try {
            Map<String, Object> map = req.getData();

            String deviceId = (String) map.get("deviceId");
            String toAddress = getWalletAddress(deviceId);

            PayModel payModel = new PayModel();
            payModel.setAmount((Integer) map.get("amount"));
            payModel.setApplyWalletAddr((String) map.get("fromAddress"));
            payModel.setDescription("收款");
            payModel.setReceiptWalletAddr(toAddress);
            payModel.setSignature(req.getSignature());
            payModel.setDeviceSn(deviceId);

            try {
                CheckDiscountPayModel cm = CheckDiscountPayModel.GetFromPayModel(payModel);
                if (cm != null) {
                    CheckDiscountPayModel cm2 = payService.queryDiscount(cm);
                    if (cm2 != null) {
                        PayModel pm2 = cm2.getPay();
                        Integer amount1 = payModel.getAmount();
                        Integer amount2 = pm2.getAmount();
                        if (amount2 != amount1) {
                            payModel.setDescription(pm2.getDescription());
                            payModel.setAttributes(String.format("{\"receivables\"=%d}", amount1));
                            payModel.setAmount(amount2);
                        }
                    }
                }
            }catch (Exception e){
                e.printStackTrace();
            }

            Integer transId = payService.createReceipt(payModel);
            if (transId > 0) {
                return PosRestUtil.success("", transId, req.getSignature());
            }
            return PosRestUtil.error(101, "收款发起失败", req.getSignature());
        }
        catch (RuntimeException re){
            log.error("pos start api", re);
            return PosRestUtil.error(102, re.getMessage(), req.getSignature());
        }
    }

    @ApiOperation(value="查询当前收款订单状态，30秒内", notes="查询当前收款订单状态，30秒内")
    @PostMapping(value="pos/check-receipt")
    public PosRestUtil queryTransactionState(@RequestBody @ApiParam(name="POS查询订单状态对象", value = "传入json格式") PosRequestUtil<Map<String,Object>> req){
        Map<String,Object> map2 = req.getData();

        String deviceId = (String)map2.get("deviceId");
        String toAddress = getWalletAddress(deviceId);

        PayModel payModel = payService.checkDoneReceipt(toAddress, 1, deviceId);
        if(null == payModel || this.billExpiredForReceipt(payModel.getUpdated())){
            return PosRestUtil.success("", null, req.getSignature());
        }
        Map<String, Object> map = new HashMap<>();
        map.put("Id", payModel.getId());
        map.put("Amount", payModel.getAmount());
        map.put("State", payModel.getState());
        map.put("ApplyWalletAddr", payModel.getApplyWalletAddr());

        return PosRestUtil.success("", map, req.getSignature());
    }

}
