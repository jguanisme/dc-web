//-----------------------------------------------------------------------------
//      @Author     : GuanJie XiaoJunfeng ShiYunling(Diana SHI)
//      @License    : GNU General Public License, version 3 (GPL-3.0)
//
//      Copyright (C)2018 GuanJie XiaoJunfeng ShiYunling(Diana SHI)
//
//      This program is open source: you can redistribute it and/or modify
//      it under the terms of the GNU General Public License as published by
//      the Free Software Foundation, either version 3 of the License, or
//      (at your option) any later version.
//
//      This program is distributed in the hope that it will be useful,
//      but WITHOUT ANY WARRANTY; without even the implied warranty of
//      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
//      GNU General Public License for more details.
//
//      You should have received a copy of the GNU General Public License
//      along with this program.If not, see < https://www.gnu.org/licenses/>.
//-----------------------------------------------------------------------------
package cn.jointcenter.wallet.api.controller.index;

import cn.jointcenter.account.service.AccountService;
import cn.jointcenter.account.service.ShopService;
import cn.jointcenter.account.service.UserService;
import cn.jointcenter.account.service.WalletService;
import cn.jointcenter.common.annotation.LoginRequired;
import cn.jointcenter.common.util.RestUtil;
import cn.jointcenter.model.BankTransactionModel;
import cn.jointcenter.wallet.api.base.BaseController;
import cn.jointcenter.wallet.api.base.UnionPayHelper;
import cn.jointcenter.wallet.service.BankTransService;
import com.unionpay.acp.sdk.*;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.*;
import java.util.concurrent.TimeUnit;

@Controller
@RequestMapping(value="/pay/union")
public class UnionPayController extends BaseController {

    private final UserService userService;
    private final AccountService accountService;
    private final WalletService walletService;
    private final ShopService shopService;
    private final BankTransService bankTransService;
    private final RedisTemplate<String, String> redisTemplate;

    @Autowired
    public UnionPayController(UserService userService, ShopService shopService,
                              AccountService accountService, WalletService walletService,
                              BankTransService bankTransService,
                              RedisTemplate<String, String> redisTemplate) {
        this.userService = userService;
        this.accountService = accountService;
        this.walletService = walletService;
        this.shopService = shopService;
        this.bankTransService = bankTransService;
        this.redisTemplate = redisTemplate;
    }

    @ApiOperation(value = "APP控件支付调起", notes = "APP控件支付调起，获得tn")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "amount", value = "金额", dataType = "Integer", paramType = "query")
    })
    @LoginRequired
    @ResponseBody
    @RequestMapping(value = "app-post", method = {RequestMethod.GET, RequestMethod.POST},
            produces = {"application/json;charset=UTF-8"})
    public RestUtil<String> appPost(Integer amount) {
        String merId = MerchantUtil.merchant_id;
        String txnAmt = String.valueOf(amount);
        String orderId = this.genOrderId();//"10000000001";
        String txnTime = MerchantUtil.getCurrentTime();
//        if(true) {
//            throw new RuntimeException("转入失败，请稍后再试");
//        }
        String rechargeSwitch = redisTemplate.opsForValue().get(this.RECHARGE_SWITCH_KEY);
        if("0".equals(rechargeSwitch)){
            throw new RuntimeException("暂不支持转入，请稍后再试");
        }

        if(amount>100000){
            throw new RuntimeException("单笔转入不能超过1000元");
        }

        String amountToday = redisTemplate.opsForValue().get(this.RECHARGE_PREFIX + this.getUTXO());
        if(StringUtils.isNotBlank(amountToday)){
            int amountAfter = amount + Integer.valueOf(amountToday);
            if(amountAfter > 500000){
                throw new RuntimeException("单日转入不能超过5000元");
            }
        }

        //start
        BankTransactionModel startModel = new BankTransactionModel();
//        startModel.setOrderId(orderId);
        startModel.setAmount(amount);
        //1 = 存钱， 2 = 取钱
        startModel.setBankTransactionType(2);
        startModel.setWalletAddr(this.getUTXO());
        startModel.setDescription("取现");

        startModel.setOrderId(bankTransService.createBankTrans(startModel));
        if(StringUtils.isEmpty(startModel.getOrderId())){
            throw new RuntimeException("订单创建失败");
        }
        orderId = startModel.getOrderId();


        if(isTesting()) {
            //发币
            BankTransactionModel exModel = new BankTransactionModel();
            exModel.setAmount(startModel.getAmount());
            exModel.setWalletAddr(startModel.getWalletAddr());
            exModel.setDescription("取现");
            exModel.setOrderId(orderId);
            BankTransactionModel exModel2 = bankTransService.createExchange(exModel);
            if (null != exModel2) {
                LogUtil.writeLog("测试发币[成功][orderId:" + orderId + "]");
                return RestUtil.error(102, "测试自动转入成功");
            } else {
                LogUtil.writeLog("测试发币[失败][orderId:" + orderId + "]");
                return RestUtil.error(102, "测试自动转入失败");
            }
        }

        Map<String, String> contentData = new HashMap<String, String>();

        /***银联全渠道系统，产品参数，除了encoding自行选择外其他不需修改***/
        contentData.put("version", MerchantUtil.version);            //版本号 全渠道默认值
        contentData.put("encoding", MerchantUtil.encoding);     //字符集编码 可以使用UTF-8,GBK两种方式
        contentData.put("signMethod", SDKConfig.getConfig().getSignMethod()); //签名方法
        contentData.put("txnType", "01");                       //交易类型 01:消费
        contentData.put("txnSubType", "01");                    //交易子类 01：消费
        contentData.put("bizType", "000201");                   //填写000201
        contentData.put("channelType", "08");                   //渠道类型 08手机

        /***商户接入参数***/
        contentData.put("merId", merId);                        //商户号码，请改成自己申请的商户号或者open上注册得来的777商户号测试
        contentData.put("accessType", "0");                     //接入类型，商户接入填0 ，不需修改（0：直连商户， 1： 收单机构 2：平台商户）
        contentData.put("orderId", orderId);                    //商户订单号，8-40位数字字母，不能含“-”或“_”，可以自行定制规则
        contentData.put("txnTime", txnTime);                    //订单发送时间，取系统时间，格式为YYYYMMDDhhmmss，必须取当前时间，否则会报txnTime无效
        contentData.put("accType", "01");                       //账号类型 01：银行卡02：存折03：IC卡帐号类型(卡介质)
        contentData.put("txnAmt", txnAmt);                      //交易金额 单位为分，不能带小数点
        contentData.put("currencyCode", "156");                 //境内商户固定 156 人民币

        // 请求方保留域，
        // 透传字段，查询、通知、对账文件中均会原样出现，如有需要请启用并修改自己希望透传的数据。
        // 出现部分特殊字符时可能影响解析，请按下面建议的方式填写：
        // 1. 如果能确定内容不会出现&={}[]"'等符号时，可以直接填写数据，建议的方法如下。
//      contentData.put("reqReserved", "透传信息1|透传信息2|透传信息3");
        // 2. 内容可能出现&={}[]"'符号时：
        // 1) 如果需要对账文件里能显示，可将字符替换成全角＆＝｛｝【】“‘字符（自己写代码，此处不演示）；
        // 2) 如果对账文件没有显示要求，可做一下base64（如下）。
        //    注意控制数据长度，实际传输的数据长度不能超过1024位。
        //    查询、通知等接口解析时使用new String(Base64.decodeBase64(reqReserved), DemoBase.encoding);解base64后再对数据做后续解析。
//      contentData.put("reqReserved", Base64.encodeBase64String("任意格式的信息都可以".toString().getBytes(DemoBase.encoding)));

        //后台通知地址（需设置为外网能访问 http https均可），支付成功后银联会自动将异步通知报文post到商户上送的该地址，【支付失败的交易银联不会发送后台通知】
        //后台通知参数详见open.unionpay.com帮助中心 下载  产品接口规范  网关支付产品接口规范 消费交易 商户通知
        //注意:1.需设置为外网能访问，否则收不到通知    2.http https均可  3.收单后台通知后需要10秒内返回http200或302状态码
        //    4.如果银联通知服务器发送通知后10秒内未收到返回状态码或者应答码非http200或302，那么银联会间隔一段时间再次发送。总共发送5次，银联后续间隔1、2、4、5 分钟后会再次通知。
        //    5.后台通知地址如果上送了带有？的参数，例如：http://abc/web?a=b&c=d 在后台通知处理程序验证签名之前需要编写逻辑将这些字段去掉再验签，否则将会验签失败
        contentData.put("backUrl", MerchantUtil.backUrl);

        /**对请求参数进行签名并发送http post请求，接收同步应答报文**/
        Map<String, String> reqData = AcpService.sign(contentData, MerchantUtil.encoding);            //报文中certId,signature的值是在signData方法中获取并自动赋值的，只要证书配置正确即可。
        String requestAppUrl = SDKConfig.getConfig().getAppRequestUrl();                                 //交易请求url从配置文件读取对应属性文件acp_sdk.properties中的 acpsdk.backTransUrl
        Map<String, String> rspData = AcpService.post(reqData, requestAppUrl, MerchantUtil.encoding);  //发送请求报文并接受同步应答（默认连接超时时间30秒，读取返回结果超时时间30秒）;这里调用signData之后，调用submitUrl之前不能对submitFromData中的键值对做任何修改，如果修改会导致验签不通过

        /**对应答码的处理，请根据您的业务逻辑来编写程序,以下应答码处理逻辑仅供参考------------->**/
        //应答码规范参考open.unionpay.com帮助中心 下载  产品接口规范  《平台接入接口规范-第5部分-附录》
        if (!rspData.isEmpty()) {
            if (AcpService.validate(rspData, MerchantUtil.encoding)) {
                LogUtil.writeLog("验证签名成功");
                String respCode = rspData.get("respCode");
                if (("00").equals(respCode)) {
                    //成功,获取tn号
                    String tn = rspData.get("tn");
                    return RestUtil.success("", tn);
                } else {
                    //其他应答码为失败请排查原因或做失败处理
                    return RestUtil.error(104, rspData.get("respMsg"));
                }
            } else {
                LogUtil.writeErrorLog("验证签名失败:" + reqData.get("respMsg"));
                return RestUtil.error(103, rspData.get("respMsg"));
            }
        } else {
            //未返回正确的http状态
            LogUtil.writeErrorLog("未获取到返回报文或返回http状态码非200");
            return RestUtil.error(102, "未获取到返回报文或返回http状态码非200");
        }
    }


    @ApiOperation(value = "银行卡验证", notes = "银行卡4要素验证")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "cardNo", value = "银行卡", dataType = "String", paramType = "query"),
            @ApiImplicitParam(name = "userName", value = "姓名", dataType = "String", paramType = "query"),
            @ApiImplicitParam(name = "phone", value = "手机号", dataType = "String", paramType = "query"),
            @ApiImplicitParam(name = "idNo", value = "身份证号", dataType = "String", paramType = "query")
    })
    @ResponseBody
    @PostMapping(value="verify-card")
    public RestUtil<String> verifyBankCard(String cardNo, String userName, String phone, String idNo){
        return UnionPayHelper.verifyBankCard(cardNo, userName, phone, idNo);
    }

    @ApiOperation(value = "银行卡信息查询", notes = "银行卡信息查询")
    @ApiImplicitParam(name = "cardNo", value = "银行卡", dataType = "String", paramType = "query")
    @ResponseBody
    @PostMapping(value="query-card")
    public RestUtil queryCardInfo(String cardNo){
        return UnionPayHelper.queryCardInfo(cardNo);
    }


    /**
     * 代付的回调
     * @param req
     * @param resp
     * @throws IOException
     */
    @RequestMapping(value="df/backRcvResponse", method = {RequestMethod.GET, RequestMethod.POST})
    public void dfBackResponse(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        LogUtil.writeLog("DfBackRcvResponse接收后台通知开始");

        String encoding = req.getParameter(SDKConstants.param_encoding);
        // 获取银联通知服务器发送的后台通知参数
        Map<String, String> reqParam = getAllRequestParam(req);
        LogUtil.printRequestLog(reqParam);

        //重要！验证签名前不要修改reqParam中的键值对的内容，否则会验签不过
        if (!AcpService.validate(reqParam, encoding)) {
            LogUtil.writeLog("代付验证签名结果[失败].");
            //验签失败，需解决验签问题

        } else {
            LogUtil.writeLog("代付验证签名结果[成功].");
            //【注：为了安全验签成功才应该写商户的成功处理逻辑】交易成功，更新商户订单状态

            String orderId = reqParam.get("orderId"); //获取后台通知的数据，其他字段也可用类似方式获取
            String respCode = reqParam.get("respCode");
            String txnTime = reqParam.get("txnTime");
            //判断respCode=00、A6后，对涉及资金类的交易，请再发起查询接口查询，确定交易成功后更新数据库。
            if("00".equals(respCode)){
                boolean checked = checkOrderStatusForDf(orderId, txnTime);
                if(checked){
                    LogUtil.writeLog("订单回调再次验证[成功][orderId:"+orderId+"]");
                    //TODO: call go api to update order
                    //start
                    BankTransactionModel upModel = bankTransService.getBankTransByOrderId(orderId);

                    if(null == upModel || upModel.getId()<1){
                        //
                        LogUtil.writeLog("订单不存在[orderId:"+orderId+"]");
                    }
                    if(null != upModel.getState() && upModel.getState() != 2){
                        // update

                        //代付确认
                        BankTransactionModel upModel2 = new BankTransactionModel();
                        upModel2.setOrderId(orderId);
                        upModel2.setDescription("存钱");
                        BankTransactionModel upModel3 = bankTransService.updateBankTrans(upModel2);
                        if(null != upModel3 && upModel3.getId()>0){
                            LogUtil.writeLog("存钱[成功][orderId:"+orderId+"]");
                        }
                        else{
                            LogUtil.writeLog("存钱[失败][orderId:"+orderId+"]");
                        }
                    }
                    else{
                        LogUtil.writeLog("订单状态不正确，无需更新");
                    }

                }
                else{
                    LogUtil.writeLog("订单回调再次验证[失败][orderId:"+orderId+"]");
                }
            }
        }
        LogUtil.writeLog("DfBackRcvResponse接收后台通知结束");
        //返回给银联服务器http 200  状态码
        resp.getWriter().print("ok");
    }

    private boolean checkOrderStatusForDf(String orderId, String txnTime) {
        String merId = MerchantUtilForDf.merchant_id;

        Map<String, String> data = new HashMap<String, String>();

        /***银联全渠道系统，产品参数，除了encoding自行选择外其他不需修改***/
        data.put("version", MerchantUtilForDf.version);                 //版本号
        data.put("encoding", MerchantUtilForDf.encoding);               //字符集编码 可以使用UTF-8,GBK两种方式
        data.put("signMethod", SDKConfigForDf.getConfig().getSignMethod()); //签名方法
        data.put("txnType", "00");                             //交易类型 00-默认
        data.put("txnSubType", "00");                          //交易子类型  默认00
        data.put("bizType", "000401");                         //业务类型 代付

        /***商户接入参数***/
        data.put("merId", merId);                  			   //商户号码，请改成自己申请的商户号或者open上注册得来的777商户号测试
        data.put("accessType", "0");                           //接入类型，商户接入固定填0，不需修改

        /***要调通交易以下字段必须修改***/
        data.put("orderId", orderId);                 //****商户订单号，每次发交易测试需修改为被查询的交易的订单号
        data.put("txnTime", txnTime);                 //****订单发送时间，每次发交易测试需修改为被查询的交易的订单发送时间

        /**请求参数设置完毕，以下对请求参数进行签名并发送http post请求，接收同步应答报文------------->**/

        Map<String, String> reqData = AcpService.sign(data, MerchantUtilForDf.encoding);			        //报文中certId,signature的值是在signData方法中获取并自动赋值的，只要证书配置正确即可。
        String url = SDKConfigForDf.getConfig().getSingleQueryUrl();										//交易请求url从配置文件读取对应属性文件acp_sdk.properties中的 acpsdk.singleQueryUrl
        Map<String, String> rspData = AcpService.post(reqData,url, MerchantUtilForDf.encoding);     //发送请求报文并接受同步应答（默认连接超时时间30秒，读取返回结果超时时间30秒）;这里调用signData之后，调用submitUrl之前不能对submitFromData中的键值对做任何修改，如果修改会导致验签不通过

        /**对应答码的处理，请根据您的业务逻辑来编写程序,以下应答码处理逻辑仅供参考------------->**/

        //应答码规范参考open.unionpay.com帮助中心 下载  产品接口规范  《平台接入接口规范-第5部分-附录》
        if(!rspData.isEmpty()){
            if(AcpService.validate(rspData, MerchantUtilForDf.encoding)){
                LogUtil.writeLog("验证签名成功");
                if(("00").equals(rspData.get("respCode"))){//如果查询交易成功
                    String origRespCode = rspData.get("origRespCode");
                    //处理被查询交易的应答码逻辑
                    if(("00").equals(origRespCode)||("A6").equals(origRespCode)){
                        //A6代付交易返回，参与清算，商户应该算成功交易，根据成功的逻辑处理
                        //交易成功，更新商户订单状态
                        //TODO
                        return true;
                    }else if(("03").equals(origRespCode)||
                            ("04").equals(origRespCode)||
                            ("05").equals(origRespCode)||
                            ("01").equals(origRespCode)||
                            ("12").equals(origRespCode)||
                            ("34").equals(origRespCode)||
                            ("60").equals(origRespCode)){
                        //订单处理中或交易状态未明，需稍后发起交易状态查询交易 【如果最终尚未确定交易是否成功请以对账文件为准】
                        //TODO
                    }else{
                        //其他应答码为交易失败
                        //TODO
                    }
                }else if(("34").equals(rspData.get("respCode"))){
                    //订单不存在，可认为交易状态未明，需要稍后发起交易状态查询，或依据对账结果为准

                }else{//查询交易本身失败，如应答码10/11检查查询报文是否正确
                    //TODO
                }
            }else{
                LogUtil.writeErrorLog("验证签名失败");
                //TODO 检查验证签名失败的原因
            }
        }else{
            //未返回正确的http状态
            LogUtil.writeLog("查询订单状态结束");
        }
        return false;
    }


    /**
     * 代收的回调
     * @param req
     * @param resp
     * @throws IOException
     */
    @RequestMapping(value="backRcvResponse", method = {RequestMethod.GET, RequestMethod.POST})
    public void backResponse(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        LogUtil.writeLog("BackRcvResponse接收后台通知开始");

        String encoding = req.getParameter(SDKConstants.param_encoding);
        // 获取银联通知服务器发送的后台通知参数
        Map<String, String> reqParam = getAllRequestParam(req);
        LogUtil.printRequestLog(reqParam);

        //重要！验证签名前不要修改reqParam中的键值对的内容，否则会验签不过
        if (!AcpService.validate(reqParam, encoding)) {
            LogUtil.writeLog("验证签名结果[失败].");
            //验签失败，需解决验签问题

        } else {
            LogUtil.writeLog("验证签名结果[成功].");
            //【注：为了安全验签成功才应该写商户的成功处理逻辑】交易成功，更新商户订单状态

            String orderId = reqParam.get("orderId"); //获取后台通知的数据，其他字段也可用类似方式获取
            String respCode = reqParam.get("respCode");
            String txnTime = reqParam.get("txnTime");
            String txnAmt = reqParam.get("txnAmt");
            //判断respCode=00、A6后，对涉及资金类的交易，请再发起查询接口查询，确定交易成功后更新数据库。
            if("00".equals(respCode)){
                boolean checked = checkOrderStatus(orderId, txnTime, txnAmt);
                if(checked){
                    LogUtil.writeLog("订单回调再次验证[成功][orderId:"+orderId+"]");
                    //TODO: call go api to update order
                    //start
                    BankTransactionModel upModel = bankTransService.getBankTransByOrderId(orderId);

                    if(null == upModel || upModel.getId()<1){
                        //
                        LogUtil.writeLog("订单不存在[orderId:"+orderId+"]");
                    }
                    if(null != upModel.getState() && upModel.getState() != 2){
                        // update
                        try{
                            //save redis today amount
                            String amountToday = redisTemplate.opsForValue().get(this.RECHARGE_PREFIX + upModel.getWalletAddr());
                            int amountAfter = upModel.getAmount();
                            if(null != amountToday) {
                                amountAfter += Integer.valueOf(amountToday);
                            }
                            redisTemplate.opsForValue().set(this.RECHARGE_PREFIX + upModel.getWalletAddr(),
                                    String.valueOf(amountAfter), this.getRemainSecondsToday(), TimeUnit.SECONDS);
                        }
                        catch (Exception ex){

                        }
                        //发币
                        BankTransactionModel exModel = new BankTransactionModel();
                        exModel.setAmount(upModel.getAmount());
                        exModel.setWalletAddr(upModel.getWalletAddr());
                        exModel.setDescription("取现");
                        exModel.setOrderId(orderId);
                        BankTransactionModel exModel2 = bankTransService.createExchange(exModel);
                        if(null != exModel2){
                            LogUtil.writeLog("发币[成功][orderId:"+orderId+"]");
                        }
                        else{
                            LogUtil.writeLog("发币[失败][orderId:"+orderId+"]");
                        }
                    }
                    else{
                        LogUtil.writeLog("订单状态不正确，无需更新");
                    }

                }
                else{
                    LogUtil.writeLog("订单回调再次验证[失败][orderId:"+orderId+"]");
                }
            }
        }
        LogUtil.writeLog("BackRcvResponse接收后台通知结束");
        //返回给银联服务器http 200  状态码
        resp.getWriter().print("ok");
    }


    private boolean checkOrderStatus(String orderId, String txnTime, String txnAmt){
        LogUtil.writeLog("查询订单状态开始");
        Map<String, String> data = new HashMap<>();

        /***银联全渠道系统，产品参数，除了encoding自行选择外其他不需修改***/
        data.put("version", MerchantUtil.version);                 //版本号
        data.put("encoding", MerchantUtil.encoding);               //字符集编码 可以使用UTF-8,GBK两种方式
        data.put("signMethod", SDKConfig.getConfig().getSignMethod()); //签名方法
        data.put("txnType", "00");                             //交易类型 00-默认
        data.put("txnSubType", "00");                          //交易子类型  默认00
        data.put("bizType", "000201");                         //业务类型 B2C网关支付，手机wap支付
//        data.put("channelType", "08");

        /***商户接入参数***/
        data.put("merId", MerchantUtil.merchant_id);                  //商户号码，请改成自己申请的商户号或者open上注册得来的777商户号测试
        data.put("accessType", "0");                           //接入类型，商户接入固定填0，不需修改 0：商户直连接入 1：收单机构接入
//        data.put("accType", "01");
        /***要调通交易以下字段必须修改***/
        data.put("orderId", orderId);                 //****商户订单号，每次发交易测试需修改为被查询的交易的订单号
        data.put("txnTime", txnTime);                 //****订单发送时间，每次发交易测试需修改为被查询的交易的订单发送时间
//        data.put("txnAmt", txnAmt);
//        data.put("currencyCode", "156");

        /**请求参数设置完毕，以下对请求参数进行签名并发送http post请求，接收同步应答报文------------->**/

        Map<String, String> reqData = AcpService.sign(data, MerchantUtil.encoding);//报文中certId,signature的值是在signData方法中获取并自动赋值的，只要证书配置正确即可。

        String url = SDKConfig.getConfig().getSingleQueryUrl();// 交易请求url从配置文件读取对应属性文件acp_sdk.properties中的 acpsdk.singleQueryUrl
        //这里调用signData之后，调用submitUrl之前不能对submitFromData中的键值对做任何修改，如果修改会导致验签不通过
        Map<String, String> rspData = AcpService.post(reqData,url, MerchantUtil.encoding);

        /**对应答码的处理，请根据您的业务逻辑来编写程序,以下应答码处理逻辑仅供参考------------->**/
        //应答码规范参考open.unionpay.com帮助中心 下载  产品接口规范  《平台接入接口规范-第5部分-附录》
        if(!rspData.isEmpty()){
            if(AcpService.validate(rspData, MerchantUtil.encoding)){
//                LogUtil.writeLog("验证签名成功");
                if("00".equals(rspData.get("respCode"))){//如果查询交易成功
                    //处理被查询交易的应答码逻辑
                    String origRespCode = rspData.get("origRespCode");
                    if("00".equals(origRespCode)){
                        //交易成功，更新商户订单状态
                        //TODO
                        return true;
                    }else if("03".equals(origRespCode) ||
                            "04".equals(origRespCode) ||
                            "05".equals(origRespCode)){
                        //需再次发起交易状态查询交易
                        //TODO
                    }else{
                        //其他应答码为失败请排查原因
                        //TODO
                    }
                }else{//查询交易本身失败，或者未查到原交易，检查查询交易报文要素
                    //TODO
                }
            }else{
                LogUtil.writeErrorLog("验证签名失败");
                //TODO 检查验证签名失败的原因
            }
        }else{
            //未返回正确的http状态
            LogUtil.writeErrorLog("未获取到返回报文或返回http状态码非200");
        }

        LogUtil.writeLog("查询订单状态结束");
        return false;
    }

    /**
     * 获取请求参数中所有的信息
     * 当商户上送frontUrl或backUrl地址中带有参数信息的时候，
     * 这种方式会将url地址中的参数读到map中，会导多出来这些信息从而致验签失败，这个时候可以自行修改过滤掉url中的参数或者使用getAllRequestParamStream方法。
     * @param request
     * @return
     */
    public static Map<String, String> getAllRequestParam(
            final HttpServletRequest request) {
        Map<String, String> res = new HashMap<String, String>();
        Enumeration<?> temp = request.getParameterNames();
        if (null != temp) {
            while (temp.hasMoreElements()) {
                String en = (String) temp.nextElement();
                String value = request.getParameter(en);
                res.put(en, value);
                // 在报文上送时，如果字段的值为空，则不上送<下面的处理为在获取所有参数数据时，判断若值为空，则删除这个字段>
                if (res.get(en) == null || "".equals(res.get(en))) {
                    // System.out.println("======为空的字段名===="+en);
                    res.remove(en);
                }
            }
        }
        return res;
    }

    /**
     * 获取请求参数中所有的信息。
     * 非struts可以改用此方法获取，好处是可以过滤掉request.getParameter方法过滤不掉的url中的参数。
     * struts可能对某些content-type会提前读取参数导致从inputstream读不到信息，所以可能用不了这个方法。理论应该可以调整struts配置使不影响，但请自己去研究。
     * 调用本方法之前不能调用req.getParameter("key");这种方法，否则会导致request取不到输入流。
     * @param request
     * @return
     */
    public static Map<String, String> getAllRequestParamStream(
            final HttpServletRequest request) {
        Map<String, String> res = new HashMap<String, String>();
        try {
            String notifyStr = new String(IOUtils.toByteArray(request.getInputStream()), MerchantUtil.encoding);
            LogUtil.writeLog("收到通知报文：" + notifyStr);
            String[] kvs= notifyStr.split("&");
            for(String kv : kvs){
                String[] tmp = kv.split("=");
                if(tmp.length >= 2){
                    String key = tmp[0];
                    String value = URLDecoder.decode(tmp[1], MerchantUtil.encoding);
                    res.put(key, value);
                }
            }
        } catch (UnsupportedEncodingException e) {
            LogUtil.writeLog("getAllRequestParamStream.UnsupportedEncodingException error: " + e.getClass() + ":" + e.getMessage());
        } catch (IOException e) {
            LogUtil.writeLog("getAllRequestParamStream.IOException error: " + e.getClass() + ":" + e.getMessage());
        }
        return res;
    }

}
