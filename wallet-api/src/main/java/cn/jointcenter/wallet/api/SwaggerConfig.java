//-----------------------------------------------------------------------------
//      @Author     : GuanJie XiaoJunfeng ShiYunling(Diana SHI)
//      @License    : GNU General Public License, version 3 (GPL-3.0)
//
//      Copyright (C)2018 GuanJie XiaoJunfeng ShiYunling(Diana SHI)
//
//      This program is open source: you can redistribute it and/or modify
//      it under the terms of the GNU General Public License as published by
//      the Free Software Foundation, either version 3 of the License, or
//      (at your option) any later version.
//
//      This program is distributed in the hope that it will be useful,
//      but WITHOUT ANY WARRANTY; without even the implied warranty of
//      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
//      GNU General Public License for more details.
//
//      You should have received a copy of the GNU General Public License
//      along with this program.If not, see < https://www.gnu.org/licenses/>.
//-----------------------------------------------------------------------------
package cn.jointcenter.wallet.api;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;

/**
 * Copyright © 2018, JointCenter
 * Author: Jeff Xiao
 * Version: 1.0
 * Date: 2018/6/11
 * Description: Swagger2 declaration
 * Others:
 */
@Configuration
public class SwaggerConfig {

    @Bean
    public Docket homeApi() {
        return new Docket(DocumentationType.SWAGGER_2)
                .groupName("0-公开接口")
                .select()
                .apis(RequestHandlerSelectors.basePackage("cn.jointcenter.wallet.api.controller.index"))
                .paths(PathSelectors.any())
                .build()
                .apiInfo(apiInfo("Digital Currency App的api文档", "首页，无需权限"));
    }

    @Bean
    public Docket userApi() {
        return new Docket(DocumentationType.SWAGGER_2)
                .groupName("1-APP接口")
                .select()
                .apis(RequestHandlerSelectors.basePackage("cn.jointcenter.wallet.api.controller.user"))
                .paths(PathSelectors.any())
                .build()
                .apiInfo(apiInfo("Digital Currency App的api文档", "用户、钱包等API"));
    }

    @Bean
    public Docket bankApi() {
        return new Docket(DocumentationType.SWAGGER_2)
                .groupName("2-银行接口")
                .select()
                .apis(RequestHandlerSelectors.basePackage("cn.jointcenter.wallet.api.controller.bank"))
                .paths(PathSelectors.any())
                .build()
                .apiInfo(apiInfo("Digital Currency 商业银行的api文档", "银行的一些API"));
    }

    @Bean
    public Docket platformApi() {
        return new Docket(DocumentationType.SWAGGER_2)
                .groupName("3-机构接口")
                .select()
                .apis(RequestHandlerSelectors.basePackage("cn.jointcenter.wallet.api.controller.platform"))
                .paths(PathSelectors.any())
                .build()
                .apiInfo(apiInfo("Digital Currency 商业机构的api文档", "平台与机构"));
    }

    @Bean
    public Docket merchantApi() {
        return new Docket(DocumentationType.SWAGGER_2)
                .groupName("4-商户接口")
                .select()
                .apis(RequestHandlerSelectors.basePackage("cn.jointcenter.wallet.api.controller.merchant"))
                .paths(PathSelectors.any())
                .build()
                .apiInfo(apiInfo("Digital Currency 商户的api文档", "商户"));
    }

    private ApiInfo apiInfo(String title, String description) {
        return new ApiInfoBuilder()
                .title(title)
                .description(description)
//                .termsOfServiceUrl("")
                .version("1.0")
                .build();
    }

}
