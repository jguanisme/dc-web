//-----------------------------------------------------------------------------
//      @Author     : GuanJie XiaoJunfeng ShiYunling(Diana SHI)
//      @License    : GNU General Public License, version 3 (GPL-3.0)
//
//      Copyright (C)2018 GuanJie XiaoJunfeng ShiYunling(Diana SHI)
//
//      This program is open source: you can redistribute it and/or modify
//      it under the terms of the GNU General Public License as published by
//      the Free Software Foundation, either version 3 of the License, or
//      (at your option) any later version.
//
//      This program is distributed in the hope that it will be useful,
//      but WITHOUT ANY WARRANTY; without even the implied warranty of
//      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
//      GNU General Public License for more details.
//
//      You should have received a copy of the GNU General Public License
//      along with this program.If not, see < https://www.gnu.org/licenses/>.
//-----------------------------------------------------------------------------
package cn.jointcenter.wallet.api.controller.index;

import cn.jointcenter.account.service.*;
import cn.jointcenter.common.util.*;
import cn.jointcenter.model.*;
import cn.jointcenter.wallet.api.base.BaseController;
import cn.jointcenter.wallet.api.base.RestCode;
import cn.jointcenter.wallet.service.BankCardService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.web.bind.annotation.*;
import org.apache.commons.lang3.StringUtils;

import javax.servlet.http.HttpServletRequest;
import java.io.*;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.*;

@Api(value="Home Controller", description="无需权限的基本操作", tags={"Home Controller"})
@RestController
@RequestMapping(value="/api", produces = {"application/json;charset=UTF-8"})
public class HomeController extends BaseController {

    private final UserService userService;
    private final AccountService accountService;
    private final WalletService walletService;
    private final ShopService shopService;
    private final BankCardService bankCardService;
    private final RedisTemplate<String, String> redisTemplate;

//    private final DbTestService dbTestService;


    @Autowired
    public HomeController(UserService userService, ShopService shopService,
                          AccountService accountService, WalletService walletService,
                          BankCardService bankCardService,
                          RedisTemplate<String, String> redisTemplate) {
        this.userService = userService;
        this.accountService = accountService;
        this.walletService = walletService;
        this.shopService = shopService;
        this.bankCardService = bankCardService;
        this.redisTemplate = redisTemplate;
    }

    @ApiOperation(value="首页", notes="测试spring boot服务")
    @ApiImplicitParam(name="version", value="版本号", dataType="String", paramType = "query")
    @GetMapping(value = {""})
    public List<String> index(HttpServletRequest request, String version){
        List<String> list = new ArrayList<>();
        list.add("Digital Currency");
        if(!StringUtils.isEmpty(version)){
            list.add("version : " + version);
        }
        LoggerFactory.getLogger(HomeController.class).debug(request.getRequestURI() + " loaded.");
        list.add("CSRF : " + this.securityUtil.generalCsrf());
        list.add("TOKEN : " + this.securityUtil.storeToken("user001"));
        list.add("LEFT : " + this.getRemainSecondsToday());


        System.out.println(ProcessUtil.simpleCommand("gmssl version"));


//        list.add("master : " + JSON.toJSONString(dbTestService.getMasterDb()));
//        list.add("slave : " + JSON.toJSONString(dbTestService.getSlaveDb()));

        return list;
    }


    @ApiOperation(value="登录，通过手机号与密码", notes="手机号与密码登录")
    @ApiImplicitParams({
            @ApiImplicitParam(name="mobile", value="手机号", dataType="String", paramType = "query"),
            @ApiImplicitParam(name="pwd", value="密码", dataType="String", paramType = "query")
    })
    @PostMapping(value = "/v2/login-mobile")
    public RestUtil loginV2(String mobile, String pwd){
        if(StringUtils.isBlank(mobile) || StringUtils.isBlank(pwd)){
            return restFail(RestCode.PARAM_ERROR);
        }

        RestUtil rest = this.checkMobile(mobile);
        if(1!=rest.getCode()){
            return rest;
        }

        AccountModel account = accountService.getByName(mobile);
        if(null == account){
            return restFail(RestCode.NO_SUCH_USER);
        }

        //compare with salt
        String calcPwd = securityUtil.encodePwd(pwd, account.getSalt());
        if(!calcPwd.equals(account.getLoginPwd())){
            return  restFail(RestCode.INCORRECT_PWD);
        }

        WalletModel walletModel = walletService.getWalletByAccount(account.getId());
        if(null == walletModel){
            return RestUtil.error(101, "钱包不存在");
        }
        String utxo = walletModel.getUtxoAddress();
        System.out.println("utxo:" + utxo);
        String token = securityUtil.storeToken(utxo+","+walletModel.getId().toString());
        Map<String, Object> map = new HashMap<>();
        map.put("token", token);
        map.put("amount", walletModel.getBalance());
        map.put("address", utxo);
        map.put("hasPwd", StringUtils.isNoneBlank(account.getLoginPwd())?1:0);
        map.put("hasPayPwd", StringUtils.isNoneBlank(account.getTradePwd())?1:0);
        map.put("freeAmount", 5000);

        String loginName = account.getName();
        if(StringUtils.isNoneBlank(loginName) && loginName.length() == 11){
            map.put("mobile", loginName);
        }
        else{
            map.put("mobile", "");
        }

        return RestUtil.success(token, map);
    }

    @ApiOperation(value="登录，通过公钥与密码", notes="公钥与密码登录")
    @ApiImplicitParams({
            @ApiImplicitParam(name="pubKey", value="公钥", dataType="String", paramType = "query"),
            @ApiImplicitParam(name="pwd", value="密码", dataType="String", paramType = "query")
    })
    @PostMapping(value = "/v2/login-pubkey")
    public RestUtil loginV2Pub(String pubKey, String pwd){
        if(StringUtils.isBlank(pubKey) || StringUtils.isBlank(pwd)){
            return restFail(RestCode.PARAM_ERROR);
        }

        WalletModel walletModel = walletService.getWalletByPubKey(pubKey);
        if(null == walletModel){
            return RestUtil.error(101, "钱包不存在");
        }

        AccountModel account = accountService.getAccount(walletModel.getAccountId());
        if(null == account){
            return restFail(RestCode.ACCOUNT_EMPTY);
        }

        //compare with salt
        String calcPwd = securityUtil.encodePwd(pwd, account.getSalt());
        if(!calcPwd.equals(account.getLoginPwd())){
            return  restFail(RestCode.INCORRECT_PWD);
        }

        String utxo = walletModel.getUtxoAddress();
        System.out.println("utxo:" + utxo);
        String token = securityUtil.storeToken(utxo+","+walletModel.getId().toString());
        Map<String, Object> map = new HashMap<>();
        map.put("token", token);
        map.put("amount", walletModel.getBalance());
        map.put("address", utxo);
        map.put("hasPwd", StringUtils.isNoneBlank(account.getLoginPwd())?1:0);
        map.put("hasPayPwd", StringUtils.isNoneBlank(account.getTradePwd())?1:0);
        map.put("freeAmount", 5000);

        String loginName = account.getName();
        if(StringUtils.isNoneBlank(loginName) && loginName.length() == 11){
            map.put("mobile", loginName);
        }
        else{
            map.put("mobile", "");
        }

        return RestUtil.success(token, map);
    }

    @ApiOperation(value="登录", notes="通过公钥获取登录token")
    @ApiImplicitParams({
            @ApiImplicitParam(name="pubKey", value="公钥", dataType="String", paramType = "query")
    })
    @Deprecated
    @PostMapping(value = "/v1/login")
    public RestUtil login(String pubKey) throws IOException {
//        UserLoginModel userLoginModel = userService.login(username, password);
//        if(null == userLoginModel){
//            return RestUtil.error(-1, "用户名不存在或者密码错误");
//        }
        WalletModel walletModel;
        String token;
        String utxo;
        Map<String, String> map;
        Map<String, Object> mapRet = new HashMap<>();
        if(StringUtils.isEmpty(pubKey)){
            map = new GmSSL().genPairKeys();
            if(StringUtils.isEmpty(map.get("pri")) || StringUtils.isEmpty(map.get("pub"))){
                return RestUtil.error(102, "公私钥生成失败");
            }
//            utxo = securityUtil.getSHA256Str(map.get("pub"));
            System.out.println("pubkey:" + map.get("pub"));
//            System.out.println("utxo:" + utxo);

            AccountModel accountModel = new AccountModel();
            accountModel.setName(securityUtil.generalSalt());
            accountModel.setZoneId(1);

            Integer accountId = accountService.createAccount(accountModel);

            if(accountId == null || accountId<=0){
                return RestUtil.error(102, "保存失败");
            }

            walletModel = new WalletModel();
            walletModel.setBalance(0);
            walletModel.setPrivKey(map.get("pri"));
            walletModel.setPubKey(map.get("pub"));
            walletModel.setAccountId(accountId);
//            walletModel.setUtxoAddress(utxo);
            Integer id = walletService.createWallet(walletModel);
            if(id<=0){
                return RestUtil.error(103, "钱包创建失败");
            }
            else{
                walletModel = walletService.getWalletByPubKey(map.get("pub"));
                utxo = walletModel.getUtxoAddress();
            }
            token = securityUtil.storeToken(utxo+","+id.toString());
            mapRet.put("token", token);
            mapRet.put("wallet", map);
            mapRet.put("amount", 0);
            mapRet.put("address", utxo);
            mapRet.put("hasPwd", 0);
            mapRet.put("hasPayPwd", 0);
            mapRet.put("freeAmount", 5000);
            mapRet.put("mobile", "");
        }
        else{
            System.out.println("pubkey:" + pubKey.replace("\\n", "\n"));
            pubKey = pubKey.replace("\\n", "\n");
//            utxo = securityUtil.getSHA256Str(pubKey.replace("\\n", "\n"));
//            walletModel = walletService.getWalletByUtxo(utxo);
            walletModel = walletService.getWalletByPubKey(pubKey);
            if(null == walletModel){
                return RestUtil.error(101, "钱包不存在");
            }
            utxo = walletModel.getUtxoAddress();
            System.out.println("utxo:" + utxo);
            token = securityUtil.storeToken(utxo+","+walletModel.getId().toString());
//            map = new HashMap<>();
            mapRet.put("token", token);
            mapRet.put("amount", walletModel.getBalance());
            mapRet.put("address", utxo);

            //get account
            AccountModel account = accountService.getAccount(walletModel.getAccountId());
            if(null == account){
//                return restFail(RestCode.ACCOUNT_EMPTY);
                //create account
                AccountModel accountModel = new AccountModel();
                accountModel.setName(securityUtil.generalSalt());
                accountModel.setZoneId(1);

                Integer accountId = accountService.createAccount(accountModel);

                if(accountId == null || accountId<=0){
                    return restFail(RestCode.STORE_ERROR);
                }
                account = accountService.getAccount(accountId);
                WalletModel walletUpdate = new WalletModel();
                walletUpdate.setId(walletModel.getId());
                walletUpdate.setAccountId(accountId);
                WalletModel saved = walletService.updateWallet(walletUpdate);
                if(null == saved){
                    return restFail(RestCode.STORE_ERROR);
                }
            }
            mapRet.put("hasPwd", StringUtils.isNoneBlank(account.getLoginPwd())?1:0);
            mapRet.put("hasPayPwd", StringUtils.isNoneBlank(account.getTradePwd())?1:0);
            mapRet.put("freeAmount", 5000);

            String loginName = account.getName();
            if(StringUtils.isNoneBlank(loginName) && loginName.length() == 11){
                mapRet.put("mobile", loginName);
            }
            else{
                mapRet.put("mobile", "");
            }

            if(StringUtils.isNoneBlank(account.getLoginPwd())){
                return restFail(RestCode.NEED_PWD);
            }

            List<CardModel> cardModels = bankCardService.getBankCardsByWallet(walletModel.getId());
            mapRet.put("cards", cardModels);
        }

        return RestUtil.success(token, mapRet);
    }

//    @ApiOperation(value="注册", notes="注册新用户")
//    @ApiImplicitParams({
//            @ApiImplicitParam(name="username", value="用户名", dataType="String", paramType = "query"),
//            @ApiImplicitParam(name="password", value="密码", dataType="String", paramType = "query")
//    })
//    @PostMapping(value="signup")
    public RestUtil signup(String username, String password){
//        UserModel userLoginModel = new UserModel(0, username, password, "", username, null);
//        Integer userId = userService.createUser(userLoginModel);
//        if(userId == null || userId<=0){
//            return RestUtil.error(500, "保存失败");
//        }
//        userLoginModel.setId(userId);
        //add wallet, add 2 accounts
//        walletService.createWallet(new WalletModel(null, username + "'s wallet",
//                0, username, userId));
//        accountService.createAccount(new AccountModel("", null, username + "'s account 1",
//                0, username + "1", "", "", userId, 3));
//        accountService.createAccount(new AccountModel("", null, username + "'s account 2",
//                0, username + "2", "", "", userId, 3));
        return RestUtil.success("", null);
    }

    @ApiOperation(value="注册商户", notes="注册商户")
    @ApiImplicitParams({
            @ApiImplicitParam(name="username", value="用户名", dataType="String", paramType = "query"),
            @ApiImplicitParam(name="password", value="密码", dataType="String", paramType = "query"),
            @ApiImplicitParam(name="walletAddress", value="钱包地址", dataType="String", paramType = "query")
    })
    @PostMapping(value="/v1/signup-merchant")
    public RestUtil signupMerchant(String username, String password, String walletAddress){
        WalletModel walletModel = walletService.getWalletByUtxo(walletAddress);
        if(null == walletModel){
            throw new RuntimeException("钱包不存在");
        }
        UserModel userModel = new UserModel();
        userModel.setName(username);
        Integer userId = userService.createUser(userModel);

        if(userId == null || userId<=0){
            return RestUtil.error(101, "保存失败");
        }

        String salt = securityUtil.generalSalt();
        String calcPwd = securityUtil.encodePwd(password, salt);
        AccountModel accountModel = new AccountModel();
        accountModel.setLoginPwd(calcPwd);
        accountModel.setSalt(salt);
        accountModel.setName(username);
        accountModel.setUserId(userId);
        accountModel.setZoneId(1);

        Integer accountId = accountService.createAccount(accountModel);

        if(accountId == null || accountId<=0){
            return RestUtil.error(102, "保存失败");
        }

        WalletModel walletUpModel = new WalletModel();
        walletUpModel.setId(walletModel.getId());
        walletUpModel.setName(username);
        walletUpModel.setAccountId(accountId);

//        add wallet, add account, add shop
        walletService.updateWallet(walletUpModel);

        shopService.createShop(new ShopModel(accountId, null, username + "'s shop",
                0, username + "'s shop", userId));
        return RestUtil.success("", null);
    }

//    @ApiOperation(value="注册机构", notes="注册机构（银行）")
//    @ApiImplicitParams({
//            @ApiImplicitParam(name="username", value="用户名", dataType="String", paramType = "query"),
//            @ApiImplicitParam(name="password", value="密码", dataType="String", paramType = "query")
//    })
//    @PostMapping(value="signup-bank")
    public RestUtil signupBank(String username, String password){
//        UserModel userLoginModel = new UserModel(0, username, password, "", username, null);
//        Integer userId = userService.createUser(userLoginModel);
//        if(userId == null || userId<=0){
//            return RestUtil.error(500, "保存失败");
//        }
//        userLoginModel.setId(userId);
//        //add account
//        accountService.createAccount(new AccountModel("", null, username + "'s account 1",
//                0, username, "", "", userId, 1));
        return RestUtil.success("", null);
    }

    @ApiOperation(value="生成密钥", notes="测试sm2生成密钥")
    @ApiImplicitParams({
            @ApiImplicitParam(name="uuid", value="设备的UUID", dataType="String", paramType = "query")
    })
    @PostMapping(value="/v1/sm2gen")
    public RestUtil sm2Gen(String uuid) throws IOException {
        String s1 = ProcessUtil.simpleCommand("gmssl genpkey -algorithm EC -pkeyopt ec_paramgen_curve:sm2p256v1 -pkeyopt ec_param_enc:named_curve -out /tmp/skey_" + uuid + ".pem");
        String s2 = ProcessUtil.simpleCommand("gmssl pkey -pubout -in /tmp/skey_" + uuid + ".pem -out /tmp/vkey_" + uuid + ".pem");

        System.out.println(s1);
        System.out.println(s2);

        String skey = null;
        Path skeyPath = java.nio.file.FileSystems.getDefault().getPath("/tmp/skey_" + uuid + ".pem");
        if(Files.exists(skeyPath)){
            for(String line : Files.readAllLines(skeyPath)){
                if(null == skey){
                    skey = line;
                }
                else{
                    skey += "\n" + line;
                }
            }
        }

        String vkey = null;
        Path vkeyPath = java.nio.file.FileSystems.getDefault().getPath("/tmp/vkey_" + uuid + ".pem");
        if(Files.exists(vkeyPath)){
            for(String line : Files.readAllLines(vkeyPath)){
                if(null == vkey){
                    vkey = line;
                }
                else{
                    vkey += "\n" + line;
                }
            }
        }

        Map<String, String> map = new HashMap<>();
        map.put("pri", skey);
        map.put("pub", vkey);
        return  RestUtil.success("", map);
    }

    @ApiOperation(value="测试签名", notes="测试sm2签名")
    @ApiImplicitParams({
            @ApiImplicitParam(name="uuid", value="设备的UUID", dataType="String", paramType = "query"),
            @ApiImplicitParam(name="text", value="签名文字", dataType="String", paramType = "query")
    })
    @PostMapping(value="/v1/sm2sign")
    public RestUtil sm2Sign(String uuid, String text) throws IOException {
        String ss = ProcessUtil.simpleCommand("echo \"" + text + "\" | gmssl pkeyutl -sign -pkeyopt ec_scheme:sm2 -inkey /tmp/skey_" + uuid + ".pem -out /tmp/sig_out_" + uuid + ".sig");
        System.out.println(ss);
        String vkey = null;
        Path vkeyPath = java.nio.file.FileSystems.getDefault().getPath("/tmp/sig_out_" + uuid + ".sig");
        byte[] bytes = Files.readAllBytes(vkeyPath);
        if(null != bytes){
            vkey = new String(Base64.getEncoder().encode(bytes));
        }

        return RestUtil.success("sm2sign for "+text, vkey);
    }

    @ApiOperation(value="测试验签", notes="测试sm2验签")
    @ApiImplicitParams({
            @ApiImplicitParam(name="uuid", value="设备的UUID", dataType="String", paramType = "query"),
            @ApiImplicitParam(name="text", value="签名文字", dataType="String", paramType = "query"),
            @ApiImplicitParam(name="sig", value="签名", dataType="String", paramType = "query")
    })
    @PostMapping(value="/v1/sm2verify")
    public RestUtil sm2Verify(String uuid, String text, String sig) throws IOException {
        Path sigPath = java.nio.file.FileSystems.getDefault().getPath("/tmp/sig_in_" + uuid + ".sig");
        if(Files.notExists(sigPath)) {
            Files.createFile(sigPath);
        }
        Files.write(sigPath, Base64.getDecoder().decode(sig));

        String v = ProcessUtil.simpleCommand("echo '" + text + "' | gmssl pkeyutl -verify -pkeyopt ec_scheme:sm2 -pubin -inkey /tmp/vkey_" + uuid + ".pem -sigfile /tmp/sig_out_" + uuid + ".sig");
        if("Signature Verified Successfully".equals(v)) {
            return RestUtil.success("sm2verify for " + text, v);
        }

        return RestUtil.error(101, v);
    }

    protected String FORCE_UPDATE_ANDROID_PREFIX = "ForceUpdate:Android";
    protected String FORCE_UPDATE_ANDROID_DESC_PREFIX = "ForceUpdate:AndroidDescription";
    protected String FORCE_UPDATE_ANDROID_URL_PREFIX = "ForceUpdate:AndroidUrl";
    /**
     * 安卓版本检测
     *
     */
    @ApiOperation(value="安卓升级", notes="安卓升级")
    @RequestMapping(value = "/v1/app-update/android", method = {RequestMethod.GET, RequestMethod.POST})
    public RestUtil<Map<String, Object>> updateAndroid() {
        return RestUtil.success("", makeUpdate(FORCE_UPDATE_ANDROID_PREFIX,
                FORCE_UPDATE_ANDROID_DESC_PREFIX,FORCE_UPDATE_ANDROID_URL_PREFIX));
    }


    protected String FORCE_UPDATE_IOS_PREFIX = "ForceUpdate:Ios";
    protected String FORCE_UPDATE_IOS_DESC_PREFIX = "ForceUpdate:IosDescription";
    protected String FORCE_UPDATE_IOS_URL_PREFIX = "ForceUpdate:IosUrl";
    /**
     * ios版本检测
     *
     * */
    @ApiOperation(value="ios升级", notes="ios升级")
    @RequestMapping(value = "/v1/app-update/ios", method = {RequestMethod.GET, RequestMethod.POST})
    public RestUtil<Map<String, Object>> updateIos() {
        return RestUtil.success("", makeUpdate(FORCE_UPDATE_IOS_PREFIX,
                FORCE_UPDATE_IOS_DESC_PREFIX,FORCE_UPDATE_IOS_URL_PREFIX));
    }

    public Map<String,Object> makeUpdate(String updatep,String descp,String urlp){
        String description="1、新版上线";
        String url="";
        String version = redisTemplate.opsForValue().get(updatep);
        if(version==null||"".equals(version)){
            version="1.0.0";
        }else{
            String d = redisTemplate.opsForValue().get(descp);
            if(d!=null&&!"".equals(d)) {
                description=d;
            }
            String u = redisTemplate.opsForValue().get(urlp);
            if(u!=null&&!"".equals(u)) {
                url=u;
            }
        }
        Map<String, Object> map = new HashMap<>();
        map.put("version", version); // 版本号
        map.put("description", description);// 升级描述
        map.put("forceUpdate", true);// 是否强制更新 true-强制 false不强制
        map.put("url", url);// apk下载地址
        return map;
    }

    @ApiOperation(value="手机号码格式验证", notes="手机号码格式验证")
    @ApiImplicitParams({
            @ApiImplicitParam(name="mobile", value="手机号", dataType="String", paramType = "query")
    })
    @PostMapping(value = "/v1/validate-mobile")
    public RestUtil validateMobile(String mobile){
        return this.checkMobile(mobile);
    }

}
