//-----------------------------------------------------------------------------
//      @Author     : GuanJie XiaoJunfeng ShiYunling(Diana SHI)
//      @License    : GNU General Public License, version 3 (GPL-3.0)
//
//      Copyright (C)2018 GuanJie XiaoJunfeng ShiYunling(Diana SHI)
//
//      This program is open source: you can redistribute it and/or modify
//      it under the terms of the GNU General Public License as published by
//      the Free Software Foundation, either version 3 of the License, or
//      (at your option) any later version.
//
//      This program is distributed in the hope that it will be useful,
//      but WITHOUT ANY WARRANTY; without even the implied warranty of
//      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
//      GNU General Public License for more details.
//
//      You should have received a copy of the GNU General Public License
//      along with this program.If not, see < https://www.gnu.org/licenses/>.
//-----------------------------------------------------------------------------
package cn.jointcenter.wallet.api.controller.user;


import cn.jointcenter.common.annotation.LoginRequired;
import cn.jointcenter.common.util.RestUtil;
import cn.jointcenter.wallet.api.base.BaseController;
import cn.jointcenter.wallet.service.CurrencyService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@Api(value="Currency Controller", description="用户持有的货币接口", tags={"User Currency Controller"})
@RestController
@RequestMapping(value="/api/v1/user/", produces = {"application/json;charset=UTF-8"})
public class UserCurrencyController extends BaseController {

    private final CurrencyService currencyService;

    @Autowired
    public UserCurrencyController(CurrencyService currencyService) {
        this.currencyService = currencyService;
    }

//    @ApiOperation(value="查询货币溯源", notes="查询货币溯源的上一个货币信息")
//    @LoginRequired
//    @GetMapping(value="currency/circulating/{currencyId}")
//    public RestUtil<List<CirculatingModel>> getCirculating(@PathVariable Integer currencyId) throws RuntimeException {
//        if(currencyId<0){
//            throw new RuntimeException("参数错误");
//        }
//        List<CirculatingModel> circulatingModels = circulatingService.findCirculatings(currencyId);
//        if(null != circulatingModels) {
//            for (CirculatingModel circulatingModel : circulatingModels) {
//                circulatingModel.setCreated(this.formatDateTimeWithZone(circulatingModel.getCreated()));
//                circulatingModel.setUpdated(this.formatDateTimeWithZone(circulatingModel.getUpdated()));
//            }
//        }
//        return RestUtil.success("", circulatingModels);
//    }

}
