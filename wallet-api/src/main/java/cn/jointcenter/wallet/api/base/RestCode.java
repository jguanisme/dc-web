//-----------------------------------------------------------------------------
//      @Author     : GuanJie XiaoJunfeng ShiYunling(Diana SHI)
//      @License    : GNU General Public License, version 3 (GPL-3.0)
//
//      Copyright (C)2018 GuanJie XiaoJunfeng ShiYunling(Diana SHI)
//
//      This program is open source: you can redistribute it and/or modify
//      it under the terms of the GNU General Public License as published by
//      the Free Software Foundation, either version 3 of the License, or
//      (at your option) any later version.
//
//      This program is distributed in the hope that it will be useful,
//      but WITHOUT ANY WARRANTY; without even the implied warranty of
//      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
//      GNU General Public License for more details.
//
//      You should have received a copy of the GNU General Public License
//      along with this program.If not, see < https://www.gnu.org/licenses/>.
//-----------------------------------------------------------------------------
package cn.jointcenter.wallet.api.base;

import lombok.Getter;

@Getter
public enum RestCode {
    OK(1, "成功"),

    PARAM_ERROR(40001, "参数不正确"),
    NEED_LOGIN(40002, "未登录或会话丢失"),
    PERMISSION_DENY(40003, "无权限"),
    STORE_ERROR(40004, "保存出错"),
    VALIDATE_ERROR(40005, "验证出错"),


    NEED_PWD(10000, "请输入登录密码"),
    NO_SUCH_USER(10001, "手机号不存在"),
    INCORRECT_PWD(10002, "登录密码不正确"),
    ACCOUNT_EMPTY(10003, "钱包的账号不存在"),
    WALLET_EMPTY(10004, "钱包不存在"),
    OLD_PWD_ERROR(10005, "原密码不正确"),
    NO_ID_AUTH(10006, "未实名认证"),
    ID_AUTH_NOT_MATCH(10007, "身份证不匹配"),
    PAY_PWD_EMPTY(10008, "支付密码未设置"),
    PAY_PWD_ERROR(10009, "支付密码不正确"),
    NAME_AUTH_NOT_MATCH(10010, "姓名不匹配"),
    BANK_CARD_EXIST(10011, "银行卡已存在"),
    MOBILE_NOT_MATCH(10012, "手机号不正确"),

    SMS_SEND_ERROR(11001, "验证码发送失败"),
    SMS_CAPTCHA_WRONG(11002, "短信验证码不正确"),

    UNKNOWN_ERROR(59999, "未知错误");


    private int code;
    private String message;

    RestCode(int code, String message) {
        this.code = code;
        this.message = message;
    }


}
