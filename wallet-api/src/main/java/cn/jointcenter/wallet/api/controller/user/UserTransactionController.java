//-----------------------------------------------------------------------------
//      @Author     : GuanJie XiaoJunfeng ShiYunling(Diana SHI)
//      @License    : GNU General Public License, version 3 (GPL-3.0)
//
//      Copyright (C)2018 GuanJie XiaoJunfeng ShiYunling(Diana SHI)
//
//      This program is open source: you can redistribute it and/or modify
//      it under the terms of the GNU General Public License as published by
//      the Free Software Foundation, either version 3 of the License, or
//      (at your option) any later version.
//
//      This program is distributed in the hope that it will be useful,
//      but WITHOUT ANY WARRANTY; without even the implied warranty of
//      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
//      GNU General Public License for more details.
//
//      You should have received a copy of the GNU General Public License
//      along with this program.If not, see < https://www.gnu.org/licenses/>.
//-----------------------------------------------------------------------------
package cn.jointcenter.wallet.api.controller.user;


import cn.jointcenter.account.service.*;
import cn.jointcenter.common.annotation.LoginRequired;
import cn.jointcenter.common.util.RestUtil;
import cn.jointcenter.model.*;
import cn.jointcenter.wallet.api.base.BaseController;
import cn.jointcenter.wallet.api.base.RestCode;
import cn.jointcenter.wallet.api.base.UnionPayHelper;
import cn.jointcenter.wallet.service.*;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Api(value="User Transaction Controller", description="用户交易相关接口: 发起交易、查询当前交易、圈存、圈提", tags={"User Transaction Controller"})
@RestController
@RequestMapping(value="/api/v1/user/", produces = {"application/json;charset=UTF-8"})
public class UserTransactionController extends BaseController {

    private final UserService userService;
    private final AccountService accountService;
    private final CurrencyService currencyService;
    private final TransactionHistoryService transactionService;
    private final WalletService walletService;
    private final PayService payService;
    private final BankTransService bankTransService;
    private final DeviceService deviceService;

    @Autowired
    public UserTransactionController(UserService userService, TransactionHistoryService transactionService,
                                     AccountService accountService, WalletService walletService,
                                     PayService payService, CurrencyService currencyService,
                                     BankTransService bankTransService, DeviceService deviceService) {
        this.userService = userService;
        this.accountService = accountService;
        this.transactionService = transactionService;
        this.walletService = walletService;
        this.payService = payService;
        this.currencyService = currencyService;
        this.bankTransService = bankTransService;
        this.deviceService = deviceService;
    }

    @ApiOperation(value="我的钱包地址", notes="我的钱包地址")
    @LoginRequired
    @GetMapping(value="pay/my-wallet-addr")
    public RestUtil myWalletAddress() throws RuntimeException {
        return RestUtil.success("", "utxo," + this.getUTXO());
    }

    @ApiOperation(value="我的付款码", notes="我的付款码")
    @LoginRequired
    @GetMapping(value="pay/my-pay-qr")
    public RestUtil myPayQr() throws RuntimeException {
        return RestUtil.success("", "pay," + this.getUTXO());
    }


    @ApiOperation(value="检查POS扫描待支付的订单", notes="检查POS扫描待支付的订单")
//    @ApiImplicitParams({
//            @ApiImplicitParam(name = "accountId", value = "账户Id", dataType = "Integer", paramType = "path")
//    })
    @LoginRequired
    @GetMapping(value="pay/check-to-pay")
    public RestUtil checkUnPay() throws RuntimeException {
        PayModel payModel = payService.checkUndoReceipt(this.getUTXO(), 0);
        if(null == payModel){
            return RestUtil.success("", null);
        }
        //检查订单是否太长，就不显示了（3m）
        if(this.billExpired(payModel.getUpdated())){
            return RestUtil.success("", null);
        }
        Map<String, Object> map = new HashMap<>();
        map.put("Id", payModel.getId());
        map.put("Amount", payModel.getAmount());
        map.put("State", payModel.getState());
        map.put("ReceiptWalletAddr", payModel.getReceiptWalletAddr());
        return RestUtil.success("", map);
    }

    @ApiOperation(value="根据ID检查订单", notes="根据ID检查订单，付款者查询")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "transId", value = "订单Id", dataType = "Integer", paramType = "query")
    })
    @LoginRequired
    @GetMapping(value="pay/check-by-id")
    public RestUtil checkPayById(Integer transId) throws RuntimeException {
        PayModel payModel = payService.checkPayStatusById(transId);
        if(null == payModel){
            return RestUtil.success("", null);
        }
        //检查订单是否太长，就不显示了（2分钟）
        if(this.billExpired(payModel.getUpdated())){
            return RestUtil.success("", null);
        }
        //只查自己的
        if(!this.getUTXO().equals(payModel.getApplyWalletAddr())){
            return RestUtil.success("", null);
        }
        Map<String, Object> map = new HashMap<>();
        map.put("Id", payModel.getId());
        map.put("Amount", payModel.getAmount());
        map.put("State", payModel.getState());
        map.put("ReceiptWalletAddr", payModel.getReceiptWalletAddr());
        return RestUtil.success("", map);
    }

    @ApiOperation(value="收款方检查30s内的订单", notes="收款方检查30s内的订单")
//    @ApiImplicitParams({
//            @ApiImplicitParam(name = "transId", value = "订单Id", dataType = "Integer", paramType = "query")
//    })
    @LoginRequired
    @GetMapping(value="pay/check-receipt")
    public RestUtil checkUnReceipt() {
//        List<PayModel> payModels = payService.findPays("0", this.getUTXO());
//        if(null == payModels || payModels.size() == 0){
//            return RestUtil.success("", null);
//        }
//        PayModel payModel = payModels.get(0);
        PayModel payModel = payService.checkDoneReceipt(this.getUTXO(), 1, "");
        if(null == payModel){
            return RestUtil.success("", null);
        }
        //检查订单是否太长，就不显示了（30s）
        if(this.billExpiredForReceipt(payModel.getUpdated())){
            return RestUtil.success("", null);
        }
        Map<String, Object> map = new HashMap<>();
        map.put("Id", payModel.getId());
        map.put("Amount", payModel.getAmount());
        map.put("State", payModel.getState());
        map.put("ApplyWalletAddr", payModel.getApplyWalletAddr());
        return RestUtil.success("", map);
    }

    @ApiOperation(value="发起转账", notes="通过扫码获得二维码，向他人账户转账")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "amount", value = "大于0的数值", dataType = "Integer", paramType = "query"),
            @ApiImplicitParam(name = "toAddress", value = "收款钱包地址", dataType = "String", paramType = "query"),
            @ApiImplicitParam(name = "deviceType", value = "收款方类型，固定为 UTXO、POS两种", dataType = "String", paramType = "query"),
            @ApiImplicitParam(name = "sig", value = "amount=&toAddress= 的签名", dataType = "String", paramType = "query"),
            @ApiImplicitParam(name = "pubKey", value = "公钥", dataType = "String", paramType = "query"),
            @ApiImplicitParam(name = "password", value = "交易密码", dataType = "String", paramType = "query")
    })
    @LoginRequired
    @PostMapping(value="pay/start")
    public RestUtil startPay(Integer amount, String toAddress, String deviceType, String sig, String pubKey, String password){
        String deviceId = "";
        if("POS".equals(deviceType)){
            //find utxo by deviceId
            DeviceModel deviceModel = deviceService.getDeviceBySN(toAddress);
            if(null == deviceModel){
                throw new RuntimeException("POS设备未注册");
            }
            WalletModel walletModel = walletService.getWalletByAccount(deviceModel.getAccountId());
            if(null == walletModel){
                throw new RuntimeException("POS设备未绑定到商户钱包");
            }
            deviceId = toAddress;
            toAddress = walletModel.getUtxoAddress();
        }
        if(!StringUtils.isBlank(password)){
            WalletModel walletModel = walletService.getWalletByUtxo(this.getUTXO());
            //verify
            AccountModel accountModel = accountService.getAccount(walletModel.getAccountId());
            if(StringUtils.isBlank(accountModel.getTradePwd())){
                return restFail(RestCode.PAY_PWD_EMPTY);
            }
            if(!accountModel.getTradePwd().equals(securityUtil.encodePwd(password,""))){
                return restFail(RestCode.PAY_PWD_ERROR);
            }
        }
        PayModel payModel = new PayModel();
        payModel.setAmount(amount);
        payModel.setApplyWalletAddr(this.getUTXO());
        payModel.setDescription("POS".equals(deviceType)?"收款":"转账");
        payModel.setReceiptWalletAddr(toAddress);
        payModel.setSignature(sig);
        payModel.setDeviceSn(deviceId);
//        payModel.setUtxoVout(pubKey);
        Integer transId = payService.createPayDirect(payModel);
        if(transId > 0){
            return RestUtil.success("", transId);
        }
        return RestUtil.error(101, "转账失败");
    }

    @ApiOperation(value="确认付款", notes="确认向商家付款")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "transId", value = "交易Id", dataType = "Integer", paramType = "query"),
            @ApiImplicitParam(name = "password", value = "交易密码", dataType = "String", paramType = "query"),
            @ApiImplicitParam(name = "sig", value = "amount=&toAddress= 的签名", dataType = "String", paramType = "query"),
            @ApiImplicitParam(name = "pubKey", value = "公钥", dataType = "String", paramType = "query")
    })
    @LoginRequired
    @PostMapping(value="pay/confirm")
    public RestUtil doPay(Integer transId, String password, String sig, String pubKey) throws RuntimeException {
        PayModel payModel = payService.checkPayStatusById(transId);
        String utxo = this.getUTXO();
        if(null == payModel || !utxo.equals(payModel.getApplyWalletAddr())){
            throw new RuntimeException("交易不存在");
        }
        if(!StringUtils.isBlank(password)){
            WalletModel walletModel = walletService.getWalletByUtxo(this.getUTXO());
            //verify
            AccountModel accountModel = accountService.getAccount(walletModel.getAccountId());
            if(StringUtils.isBlank(accountModel.getTradePwd())){
                return restFail(RestCode.PAY_PWD_EMPTY);
            }
            if(!accountModel.getTradePwd().equals(securityUtil.encodePwd(password,""))){
                return restFail(RestCode.PAY_PWD_ERROR);
            }
        }
        Boolean state = payService.confirmReceipt(transId, utxo, sig, pubKey);
        if(!state){
            throw new RuntimeException("支付失败");
        }
        Integer ret = payService.confirmDiscount(payModel);
        if(ret==null){

        }
        return RestUtil.success("", transId);
    }

    @ApiOperation(value="查询交易记录", notes="查询指定账号下的交易记录")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "page", value = "页码", dataType = "Integer", paramType = "query")
    })
    @LoginRequired
    @GetMapping(value="trans/list")
    public RestUtil<List<TransactionHistoryModel>> getTransactionsByAccount(Integer page) {
        if(null == page || page < 1){
            page = 1;
        }

        List<TransactionHistoryModel> transactionModels = transactionService.findTransactions(this.getUTXO(), page, 10, null);
        return RestUtil.success("", transactionModels);
    }

//    @ApiOperation(value="转入操作", notes="向指定账号转入金额（圈存）")
//    @ApiImplicitParams({
//            @ApiImplicitParam(name = "accountId", value = "账户Id", dataType = "Integer", paramType = "path"),
//            @ApiImplicitParam(name = "amount", value = "大于0的数值，小数点后最多2位", dataType = "Double", paramType = "query"),
//            @ApiImplicitParam(name = "password", value = "交易密码", dataType = "String", paramType = "query")
//    })
//    @LoginRequired
//    @PostMapping(value="trans/due-in/{accountId}")
//    public RestUtil<Integer> doRecharge(@PathVariable Integer accountId, Double amount, String password){
//        InvalidCurrencyModel exchangingModel = new InvalidCurrencyModel();
//        exchangingModel.setAccountId(accountId);
//        exchangingModel.setAmount(amount);
//        exchangingModel.setDesc("转入");
//        return RestUtil.success("", exchangeService.createExchange(exchangingModel));
//    }

    @ApiOperation(value="转出操作", notes="从指定账号转出金额（圈提）")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "idNo", value = "身份证", dataType = "String", paramType = "query"),
            @ApiImplicitParam(name = "cardNo", value = "银行卡", dataType = "String", paramType = "query"),
            @ApiImplicitParam(name = "amount", value = "大于0的整数，单位：分", dataType = "Integer", paramType = "query"),
            @ApiImplicitParam(name = "password", value = "交易密码", dataType = "String", paramType = "query")
    })
    @LoginRequired
    @PostMapping(value="trans/due-out")
    public RestUtil doWithdraw(String idNo, String cardNo, Integer amount, String password) throws RuntimeException {
        BankTransactionModel bankTransactionModel = new BankTransactionModel();
        bankTransactionModel.setAmount(amount);
        bankTransactionModel.setWalletAddr(this.getUTXO());
        bankTransactionModel.setDescription("存钱");
        if(!StringUtils.isBlank(password)){
            WalletModel walletModel = walletService.getWalletByUtxo(this.getUTXO());
            //verify
            AccountModel accountModel = accountService.getAccount(walletModel.getAccountId());
            if(StringUtils.isBlank(accountModel.getTradePwd())){
                return restFail(RestCode.PAY_PWD_EMPTY);
            }
            if(!accountModel.getTradePwd().equals(securityUtil.encodePwd(password,""))){
                return restFail(RestCode.PAY_PWD_ERROR);
            }
        }
//        bankTransactionModel.setOrderId(this.genOrderId());
        String orderId = bankTransService.createDeposit(bankTransactionModel);
        if(StringUtils.isEmpty(orderId)){
            throw new RuntimeException("操作失败");
        }

        bankTransactionModel.setOrderId(orderId);

        //start
//        BankTransactionModel startModel = new BankTransactionModel();
//        startModel.setOrderId(this.genOrderId());
//        startModel.setAmount(amount);
//        //1 = 存钱， 2 = 取钱
//        startModel.setBankTransactionType(1);
//        startModel.setWalletAddr(this.getUTXO());
//
//        startModel.setId(bankTransService.createBankTrans(startModel));
        //get
        //TODO: update in callback
//        BankTransactionModel upModel = new BankTransactionModel();
//        upModel.setOrderId(bankTransactionModel.getOrderId());
//        upModel.setDescription("存钱");
//        bankTransService.updateBankTrans(upModel);

//        boolean ret = UnionPayHelper.startDaiFu(orderId, idNo, cardNo, amount);
//
//        if(ret) {
        return RestUtil.success("存钱申请成功, T+1交易, 请耐心等待", true);
//        }
//        else{
//            throw new RuntimeException("存钱申请失败");
//        }
    }

}
