# 项目dc-web
## 项目说明
dc-web是数字货币java的aip工程

wallet-api是代码目录

wallet-api-test是测试代码

## 编译方法
工程采用grandle编译，编译方法：

windows: gradlew.bat build

linux: ./gradlew build

编译结果在wallet-api/build/libs目录下

## 代码说明
dc-web是标准的grandle工程

controller包是接口定义

base包是银联支付相关

adapter包是登录控制

wallet-api只是接口转换，所有的业务实现都在golang实现的dc-gateway中

this project is protected by gpl.txt

