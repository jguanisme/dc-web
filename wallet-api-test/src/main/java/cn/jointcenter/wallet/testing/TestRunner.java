package cn.jointcenter.wallet.testing;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import javafx.application.Application;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.boot.autoconfigure.orm.jpa.HibernateJpaAutoConfiguration;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.*;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

@SpringBootApplication(exclude = {
        DataSourceAutoConfiguration.class,
        HibernateJpaAutoConfiguration.class
})
public class TestRunner {
    @Value("${dc.gateway.test-domain}")
    private String testDomain;

    @Value("${dc.gateway.test-reg-count}")
    private Integer testRegCount;

    @Value("${dc.gateway.test-run-count}")
    private Integer testRunCount;

    @Value("${dc.gateway.test-run-concurrent}")
    private Integer testRunConcurrent;

    public static void main (String[] args) throws IOException {
        SpringApplication.run(TestRunner.class, args);
    }

    @Bean
    public CommandLineRunner commandLineRunner(ApplicationContext ctx) {
        return args -> {
            TestRunner dcTest = new TestRunner();
            if(args.length == 0){
                System.out.println("args invalid!");
                return;
            }
            String m = args[0];
            System.out.println("action {"+m+"} firing..");
            if("reg".equals(m)){
                dcTest.runRegBatch(testDomain, testRegCount);
                SpringApplication.exit(ctx);
                return;
            }
            if("login".equals(m)){
                dcTest.runLoginBatch(testDomain);
                SpringApplication.exit(ctx);
                return;
            }
            if("recharge".equals(m)){
                dcTest.runRecharge(testDomain, testRunCount, testRunConcurrent);
//                SpringApplication.exit(ctx);
                return;
            }
            if("trans".equals(m)) {
                if(args.length < 2){
                    System.out.println("to address lost!");
                    return;
                }
                dcTest.runTrans(testDomain, args[1], testRunCount, testRunConcurrent);
//                SpringApplication.exit(ctx);
            }
        };
    }

    private void runRegBatch(String url, Integer regCount) throws IOException {
        List<String> list = new ArrayList<>();
        //循环注册1000个
        for(int i=0;i<regCount;i++){
            String cmd = "curl -X POST "+
                    " '"+url+"api/v1/login' "+
                    "-H 'Postman-Token: 3ff95942-db43-43a8-ad37-cebf5205d6f9' "+
                    "-H 'accept: application/json;charset=UTF-8' "+
                    "-H 'cache-control: no-cache'";
            System.out.print(cmd);
            list.add(simpleCommand(cmd));
        }

        String content = JSON.toJSONString(list);
        Path path = java.nio.file.FileSystems.getDefault().getPath("/tmp/dc_reg.txt");
        if(Files.exists(path)){
            Files.delete(path);
        }
        Files.write(path, content.getBytes());
    }

    private void runLoginBatch(String url) throws IOException {
        Path path = java.nio.file.FileSystems.getDefault().getPath("/tmp/dc_reg.txt");
        if(!Files.exists(path)){
            System.out.println("reg file not exists");
            return;
        }
        String skey = null;
        for(String line : Files.readAllLines(path)){
            if(null == skey){
                skey = line;
            }
            else{
                skey += "" + line;
            }
        }
        if(null == skey){
            System.out.println("reg file is empty");
            return;
        }
        List<String[]> list = new ArrayList<>();
        JSONArray jArr = JSON.parseArray(skey);
        for(Object item : jArr){
            String strItem = (String)item;
            JSONObject jObj = JSON.parseObject(strItem).getJSONObject("result");
            String addr = jObj.getString("address");
            String pub = jObj.getJSONObject("wallet").getString("pub");
            String pri = jObj.getJSONObject("wallet").getString("pri");
            String cmd = "curl -X POST " +
                    "  '"+url+"api/v1/login?pubKey="+parsePub(pub)+"' " +
                    "  -H 'Postman-Token: f62563a4-7a3c-437d-9a75-f2ccb55a1786' " +
                    "  -H 'accept: application/json;charset=UTF-8' " +
                    "  -H 'cache-control: no-cache'";
            System.out.print(cmd);
            String ret = simpleCommand(cmd);
            JSONObject retObj = JSON.parseObject(ret);
            if(retObj.getInteger("code") != 1){
                continue;
            }
            String token = retObj.getJSONObject("result").getString("token");
            list.add(new String[]{addr, pub, pri, token});
        }
        Path path2 = java.nio.file.FileSystems.getDefault().getPath("/tmp/dc_login.txt");
        if(Files.exists(path2)){
            Files.delete(path2);
        }
        Files.write(path2, JSON.toJSONString(list).getBytes());
    }

    private void runRecharge(String url, int runCount, int runConcurrent) throws IOException {
        Path path = java.nio.file.FileSystems.getDefault().getPath("/tmp/dc_login.txt");
        if (!Files.exists(path)) {
            System.out.println("login file not exists");
            return;
        }
        String skey = null;
        for (String line : Files.readAllLines(path)) {
            if (null == skey) {
                skey = line;
            } else {
                skey += "" + line;
            }
        }
        if (null == skey) {
            System.out.println("login file is empty");
            return;
        }
        List<String> list = new ArrayList<>();
        JSONArray jArr = JSON.parseArray(skey);
        if (runConcurrent <= 0) {
            runConcurrent = 1;
        }
        if (runCount <= 0) {
            runCount = jArr.size();
        }
        for (int i = 0; i < runConcurrent; i++) {
            final int ix = i;
            final int max = runCount / runConcurrent;
            new Thread(() -> {
                int s = ix * max;
                if (s + max > jArr.size()) {
                    s = jArr.size() - max;
                }
                if (s < 0) {
                    s = 0;
                }
                for (int y = s; y < s + max; y++) {
                    int tempIx = y;
                    if (y >= jArr.size()) {
                        tempIx = (y - jArr.size()) % jArr.size();
                    }
                    Object item = jArr.get(tempIx);
                    JSONArray arr1 = (JSONArray)item;
                    String pub = arr1.getString(1);
                    String token = arr1.getString(3);
                    String cmd = "curl -X POST " +
                            "  '" + url + "pay/union/app-post?amount=100000' " +
                            "  -H 'Postman-Token: 10aa92c7-740b-4f69-9a32-e58eee431d21' " +
                            "  -H 'accept: application/json;charset=UTF-8' " +
                            "  -H 'cache-control: no-cache' " +
                            "  -H 'token: " + token + "'";
                    System.out.print(cmd);
                    String ret = simpleCommand(cmd);
                    list.add(ret);
                }
                Path path2 = java.nio.file.FileSystems.getDefault().getPath("/tmp/dc_recharge_" + String.valueOf(ix) + ".txt");
                try {
                    if (Files.exists(path2)) {
                        Files.delete(path2);
                    }
                    Files.write(path2, JSON.toJSONString(list).getBytes());
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }).start();
        }
    }

    private void runTrans (String url, final String toAddr,int runCount, int runConcurrent) throws IOException {
        Path path = java.nio.file.FileSystems.getDefault().getPath("/tmp/dc_login.txt");
        if (!Files.exists(path)) {
            System.out.println("login file not exists");
            return;
        }
        String skey = null;
        for (String line : Files.readAllLines(path)) {
            if (null == skey) {
                skey = line;
            } else {
                skey += "" + line;
            }
        }
        if (null == skey) {
            System.out.println("login file is empty");
            return;
        }
        List<String> list = new ArrayList<>();
        final JSONArray jArr = JSON.parseArray(skey);
        if (runConcurrent <= 0) {
            runConcurrent = 1;
        }
        if (runCount <= 0) {
            runCount = jArr.size();
        }
        for (int i = 0; i < runConcurrent; i++) {
            final int ix = i;
            final int max = runCount / runConcurrent;
            new Thread(() -> {
                int s = ix * max;
                if (s + max > jArr.size()) {
                    s = jArr.size() - max;
                }
                if (s < 0) {
                    s = 0;
                }
                for (int y = s; y < s + max; y++) {
                    int tempIx = y;
                    if (y >= jArr.size()) {
                        tempIx = (y - jArr.size()) % jArr.size();
                    }
                    Object item = jArr.get(tempIx);
                    JSONArray arr1 = (JSONArray) item;
                    String pub = arr1.getString(1);
                    String token = arr1.getString(3);
                    String cmd = "curl -X POST " +
                            "  '" + url + "api/v1/user/pay/start?amount=100&deviceType=" + parsePub(pub) + "&toAddress=" + toAddr + "' " +
                            "  -H 'Postman-Token: afd63e10-de02-4746-8740-1760b70d2772' " +
                            "  -H 'accept: application/json;charset=UTF-8' " +
                            "  -H 'cache-control: no-cache' " +
                            "  -H 'token: " + token + "'";
                    System.out.print(cmd);
                    String ret = simpleCommand(cmd);
                    list.add(ret);
                }
                Path path2 = java.nio.file.FileSystems.getDefault().getPath("/tmp/dc_trans_" + String.valueOf(ix) + ".txt");
                try {
                    if (Files.exists(path2)) {
                        Files.delete(path2);
                    }
                    Files.write(path2, JSON.toJSONString(list).getBytes());
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }).start();
        }
    }

    private String parsePub(String pub){
        if(null == pub){
            return "";
        }
        try {
            //.replaceAll(" ", "%20")
            String os = System.getProperty("os.name");
            if(os.toLowerCase().startsWith("win")) {
                return pub.replace("+", "%2B").replaceAll(" ", "%20")
                        .replaceAll("\\n", "%0A")
                        .replace("/", "%2F");
            }
            return URLEncoder.encode(pub.replaceAll("\\n", "\\\\n"), "utf8");
        } catch (Exception e) {
            e.printStackTrace();
            return "";
        }
    }

    private String simpleCommand(String cmd){
        try {
            String[] cmdArray = {
                    "/bin/sh",
                    "-c",
                    cmd
            };
            String os = System.getProperty("os.name");
            if(os.toLowerCase().startsWith("win")){
                cmdArray[0] = "cmd.exe";
                cmdArray[1] = "/c";
             }
            Process process = Runtime.getRuntime().exec(cmdArray);
            process.waitFor();
            InputStreamReader isr = new InputStreamReader(process.getInputStream());
            BufferedReader buf = new BufferedReader(isr);
            String line = "";
            StringBuilder output = null;
            while ((line = buf.readLine()) != null) {
                if(null == output){
                    output = new StringBuilder(line);
                }
                else {
                    output.append("").append(line);
                }
            }
            process.destroy();
            if(null == output){
                return "";
            }
            return output.toString();
        } catch (IOException | InterruptedException e) {
            e.printStackTrace();
        }
        return "";
    }
}
